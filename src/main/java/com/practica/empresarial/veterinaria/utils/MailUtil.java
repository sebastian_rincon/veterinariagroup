package com.practica.empresarial.veterinaria.utils;

import com.practica.empresarial.veterinaria.exceptions.MessageException;
import com.practica.empresarial.veterinaria.model.dto.MailBodyDto;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Component
@Slf4j
public class MailUtil {

    private final MessageUtil messageUtil;
    private final JavaMailSender mailSender;
    private final FreeMarkerConfigurer freeMarkerConfigurer;
    private MimeMessage message;
    private MimeMessageHelper messageHelper;

    @Autowired
    public MailUtil(JavaMailSender mailSender, MessageUtil messageUtil, FreeMarkerConfigurer freeMarkerConfigurer) {
        this.mailSender = mailSender;
        this.messageUtil = messageUtil;
        this.freeMarkerConfigurer = freeMarkerConfigurer;
    }


    public void sendEmailWithAttachment(MailBodyDto mail, String nameFile, FileSystemResource file) {
        try {
            message = mailSender.createMimeMessage();
            messageHelper = new MimeMessageHelper(message, true);
            messageHelper.setTo(mail.getEmail());
            messageHelper.setText(templateEmail(mail.getContent()), true);
            messageHelper.setSubject(mail.getSubject());
            messageHelper.addAttachment(nameFile, file);
            mailSender.send(message);
            log.info(messageUtil.resolveMessageSource("successful.send.mail"));
        } catch (MessagingException e) {
            throw new MessageException(messageUtil.resolveMessageSource("failed.send.mail"));
        }
    }

    public void sendEmailWithoutAttachment(MailBodyDto mail) {
        try {
            message = mailSender.createMimeMessage();
            messageHelper = new MimeMessageHelper(message);
            messageHelper.setTo(mail.getEmail());
            messageHelper.setText(this.templateEmail(mail.getContent()), true);
            messageHelper.setSubject(mail.getSubject());
            mailSender.send(message);
            log.info(messageUtil.resolveMessageSource("successful.send.mail"));
        } catch (MessagingException e) {
            throw new MessageException(messageUtil.resolveMessageSource("failed.send.mail"));
        }
    }

    private String templateEmail(String content) {
        try {
            Map<String, Object> data = new HashMap<>();
            data.put("content", content);
            Template template = freeMarkerConfigurer.createConfiguration()
                    .getTemplate("template-email.ftl");
            return FreeMarkerTemplateUtils.processTemplateIntoString(template, data);
        } catch (IOException | TemplateException e) {
            throw new MessageException(messageUtil.resolveMessageSource("failed.get.template.email"));
        }
    }
}
