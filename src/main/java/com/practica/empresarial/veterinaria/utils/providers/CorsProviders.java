package com.practica.empresarial.veterinaria.utils.providers;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@Data
public class CorsProviders {

    @Value("${cors.filter.origin}")
    private String origin;

    @Value("${cors.filter.headers}")
    private String[] headers;

    @Value("${cors.filter.methods}")
    private String[] methods;

}
