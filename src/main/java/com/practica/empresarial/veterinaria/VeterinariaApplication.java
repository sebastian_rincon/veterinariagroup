package com.practica.empresarial.veterinaria;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
public class VeterinariaApplication implements WebMvcConfigurer {

    public static void main(String[] args) {
        SpringApplication.run(VeterinariaApplication.class, args);
    }
}
