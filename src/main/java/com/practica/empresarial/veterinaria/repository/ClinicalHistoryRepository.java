package com.practica.empresarial.veterinaria.repository;

import com.practica.empresarial.veterinaria.model.ClinicalHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClinicalHistoryRepository extends JpaRepository<ClinicalHistory, Long> {
}
