package com.practica.empresarial.veterinaria.repository;

import com.practica.empresarial.veterinaria.model.Pet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PetRepository extends JpaRepository<Pet, Long> {

    @Query(nativeQuery = true,
            value = "SELECT m.id_mascota, " +
                    "m.nombre_mascota, " +
                    "m.especie_mascota, " +
                    "m.edad_mascota, " +
                    "m.raza_mascota, " +
                    "m.sexo_mascota, " +
                    "m.observacion_mascota, " +
                    "u.id_usuario, " +
                    "m.estado_mascota " +
                    "FROM mascota AS m " +
                    "INNER JOIN usuario AS u ON u.id_usuario = m.id_usuario " +
                    "WHERE u. documento_usuario =:identificacion")
    List<Pet> findAllByDocumentUser(@Param(value = "identificacion") String document);
}
