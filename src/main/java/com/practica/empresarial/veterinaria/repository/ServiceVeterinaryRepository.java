package com.practica.empresarial.veterinaria.repository;

import com.practica.empresarial.veterinaria.model.ServiceVeterinary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ServiceVeterinaryRepository extends JpaRepository<ServiceVeterinary, Long> {

    List<ServiceVeterinary> findAllByNameService(String nameService);

    @Query(nativeQuery = true,
            value = "SELECT " +
                    "s.id_servicio, " +
                    "s.nombre_servicio, " +
                    "s.cantidad_servicio," +
                    "s.precio_servicio, " +
                    "s.descripcion_servicio, " +
                    "s.estado_servicio " +
                    "FROM servicio AS s " +
                    "INNER JOIN categoria_servicio AS cs " +
                    "           on s.id_servicio = cs.id_servicio " +
                    "WHERE cs.id_categoria =:category")
    List<ServiceVeterinary> findAllByIdCategory(@Param(value = "category") Integer idCategory);

}
