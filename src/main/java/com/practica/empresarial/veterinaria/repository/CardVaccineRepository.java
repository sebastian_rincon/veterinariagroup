package com.practica.empresarial.veterinaria.repository;

import com.practica.empresarial.veterinaria.model.CardVaccine;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CardVaccineRepository extends JpaRepository<CardVaccine, Long> {

    Optional<CardVaccine> findByIdPet(Integer idPet);

}
