package com.practica.empresarial.veterinaria.repository;

import com.practica.empresarial.veterinaria.model.Egress;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EgressRepository extends JpaRepository<Egress, Long> {

    List<Egress> findAllByType(String type);

    List<Egress> findAllByIdPet(Long idPet);
}
