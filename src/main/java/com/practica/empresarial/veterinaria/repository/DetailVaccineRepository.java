package com.practica.empresarial.veterinaria.repository;

import com.practica.empresarial.veterinaria.model.DetailVaccine;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DetailVaccineRepository extends JpaRepository<DetailVaccine, Long> {

    List<DetailVaccine> findAllByIdCardVaccine(Integer idCardVaccine);
}
