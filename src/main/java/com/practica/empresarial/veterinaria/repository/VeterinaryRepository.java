package com.practica.empresarial.veterinaria.repository;

import com.practica.empresarial.veterinaria.model.Veterinary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VeterinaryRepository extends JpaRepository<Veterinary, Long> {

    List<Veterinary> findAllByName(String name);
}
