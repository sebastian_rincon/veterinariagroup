package com.practica.empresarial.veterinaria.service;

import com.practica.empresarial.veterinaria.configuration.JwtToken;
import com.practica.empresarial.veterinaria.exceptions.AuthException;
import com.practica.empresarial.veterinaria.model.UserVeterinary;
import com.practica.empresarial.veterinaria.model.dto.LoginRequest;
import com.practica.empresarial.veterinaria.model.dto.SuccessLoginDto;
import com.practica.empresarial.veterinaria.model.dto.UserVeterinaryDto;
import com.practica.empresarial.veterinaria.model.transform.UserVeterinaryTransform;
import com.practica.empresarial.veterinaria.repository.UserVeterinaryRepository;
import com.practica.empresarial.veterinaria.utils.MessageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class AuthService {

    private final UserVeterinaryRepository userVeterinaryRepository;
    private final UserVeterinaryTransform userVeterinaryTransform;
    private final AuthenticationManager authenticationManager;
    private final JwtToken jwtTokenUtil;
    private final JwtUserDetailsService jwtInMemoryUserDetailsService;
    private final MessageUtil messageUtil;

    @Autowired
    public AuthService(UserVeterinaryRepository userVeterinaryRepository, UserVeterinaryTransform userVeterinaryTransform, AuthenticationManager authenticationManager, JwtToken jwtTokenUtil, JwtUserDetailsService jwtInMemoryUserDetailsService, MessageUtil messageUtil) {
        this.userVeterinaryRepository = userVeterinaryRepository;
        this.userVeterinaryTransform = userVeterinaryTransform;
        this.authenticationManager = authenticationManager;
        this.jwtTokenUtil = jwtTokenUtil;
        this.jwtInMemoryUserDetailsService = jwtInMemoryUserDetailsService;
        this.messageUtil = messageUtil;
    }


    public SuccessLoginDto generateAuthenticationToken(LoginRequest login) {
        authenticate(login.getUsername(), login.getPassword());

        final UserDetails userDetails = jwtInMemoryUserDetailsService
                .loadUserByUsername(login.getUsername());

        final String token = jwtTokenUtil.generateToken(userDetails);

        return new SuccessLoginDto(token);
    }

    public UserVeterinaryDto registerUser(UserVeterinaryDto user) {
        UserVeterinary newUser = userVeterinaryTransform.userDtoToUser(user);
        return userVeterinaryTransform.userToUserDto(userVeterinaryRepository.save(newUser));
    }

    private void authenticate(String username, String password) {
        Objects.requireNonNull(username);
        Objects.requireNonNull(password);

        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new AuthException(messageUtil.resolveMessageSource("user.disable"));
        } catch (BadCredentialsException e) {
            throw new AuthException(messageUtil.resolveMessageSource("credential.invalid"));
        }
    }
}
