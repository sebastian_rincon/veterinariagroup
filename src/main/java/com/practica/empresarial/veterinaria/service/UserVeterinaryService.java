package com.practica.empresarial.veterinaria.service;

import com.practica.empresarial.veterinaria.exceptions.UserException;
import com.practica.empresarial.veterinaria.model.UserVeterinary;
import com.practica.empresarial.veterinaria.model.dto.UserVeterinaryDto;
import com.practica.empresarial.veterinaria.model.transform.UserVeterinaryTransform;
import com.practica.empresarial.veterinaria.repository.UserVeterinaryRepository;
import com.practica.empresarial.veterinaria.utils.MessageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserVeterinaryService {

    private final UserVeterinaryRepository userVeterinaryRepository;
    private final MessageUtil messageUtil;
    private final UserVeterinaryTransform userVeterinaryTransform;

    @Autowired
    public UserVeterinaryService(UserVeterinaryRepository userVeterinaryRepository,
                                 MessageUtil messageUtil,
                                 UserVeterinaryTransform userVeterinaryTransform) {
        this.userVeterinaryRepository = userVeterinaryRepository;
        this.messageUtil = messageUtil;
        this.userVeterinaryTransform = userVeterinaryTransform;
    }


    public UserVeterinaryDto getUserByUserName(String userName) {
        try {
            UserVeterinary userVeterinary = userVeterinaryRepository.findByUserName(userName).orElse(null);
            return userVeterinaryTransform.userToUserDto(userVeterinary);
        } catch (Exception e) {
            throw new UserException(messageUtil.resolveMessageSource("fieldRequire.exception.message"));
        }
    }


}
