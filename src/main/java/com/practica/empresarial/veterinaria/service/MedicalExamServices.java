package com.practica.empresarial.veterinaria.service;

import com.itextpdf.text.pdf.PdfPTable;
import com.practica.empresarial.veterinaria.exceptions.MedicalExamException;
import com.practica.empresarial.veterinaria.exceptions.VeterinaryException;
import com.practica.empresarial.veterinaria.model.dto.ExamDetailDto;
import com.practica.empresarial.veterinaria.model.dto.MailBodyDto;
import com.practica.empresarial.veterinaria.model.dto.MedicalExamDto;
import com.practica.empresarial.veterinaria.model.transform.MedicalExamTransform;
import com.practica.empresarial.veterinaria.repository.MedicalExamRepository;
import com.practica.empresarial.veterinaria.utils.MailUtil;
import com.practica.empresarial.veterinaria.utils.MessageUtil;
import com.practica.empresarial.veterinaria.utils.PdfUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class MedicalExamServices {
    private final MessageUtil messageUtil;
    private final MedicalExamRepository medicalExamRepository;
    private final MedicalExamTransform medicalExamTransform;
    private final ExamDetailService examDetailService;
    private final MailUtil mailUtil;
    private final PdfUtil pdfUtil;

    @Autowired
    public MedicalExamServices(
            MedicalExamRepository medicalExamRepository,
            MedicalExamTransform medicalExamTransform,
            MessageUtil messageUtil,
            ExamDetailService examDetailService,
            MailUtil mailUtil,
            PdfUtil pdfUtil) {
        this.medicalExamRepository = medicalExamRepository;
        this.medicalExamTransform = medicalExamTransform;
        this.messageUtil = messageUtil;
        this.examDetailService = examDetailService;
        this.mailUtil = mailUtil;
        this.pdfUtil = pdfUtil;
    }

    public List<MedicalExamDto> getAllMedicalExamByPet(Long pet) {
        if (null != pet) {
            return medicalExamRepository.getAllByPet(pet)
                    .stream()
                    .map(medicalExamTransform::medicalExamToMedicalExamDto)
                    .collect(Collectors.toList());
        }

        throw new MedicalExamException(messageUtil.resolveMessageSource("fieldRequire.exception.message"));
    }

    public String sendByEmailMedicalExamReport(String userEmail, Long exam) {
        if (StringUtils.isNotBlank(userEmail) && null != exam) {

            String title = getNamePdf(exam);

            MailBodyDto emailBody = MailBodyDto.builder()
                    .email(userEmail)
                    .content("Reporte examenes médicos se adjunta archivo PDF")
                    .subject("examen medico")
                    .build();

            FileSystemResource file = new FileSystemResource(title);
            mailUtil.sendEmailWithAttachment(emailBody, title, file);
            return messageUtil.resolveMessageSource("successful.send.mail");
        }
        throw new MedicalExamException(messageUtil.resolveMessageSource("fieldRequire.exception.message"));
    }

    private MedicalExamDto getOneById(Long id) {
        if (null != id) {
            return medicalExamTransform.medicalExamToMedicalExamDto(medicalExamRepository.getOneByIdMedicalExam(id));
        }
        throw new MedicalExamException(messageUtil.resolveMessageSource("fieldRequire.exception.message"));
    }

    private List<ExamDetailDto> getAllExamDetailByIdExam(Long id) {
        if (null != id) {
            return examDetailService.getAllExamDetailByIdExam(id);
        }
        throw new MedicalExamException(messageUtil.resolveMessageSource("fieldRequire.exception.message"));
    }

    private String getNamePdf(Long exam) throws VeterinaryException {
        MedicalExamDto medExam = getOneById(exam);
        return pdfUtil.generatePdf(
                "examen_medico_mascota.pdf",
                generateTableHeader(medExam),
                generateTableContent(exam),
                generateTableFooter(medExam));
    }

    private PdfPTable generateTableHeader(MedicalExamDto medExam) {

        PdfPTable tableHeader = new PdfPTable(1);
        tableHeader.addCell("Mascota: " + medExam.getPet() + "\n" + "Tipo de examen:" + medExam.getType());
        return tableHeader;
    }

    private PdfPTable generateTableFooter(MedicalExamDto medExam) {

        PdfPTable tableFooter = new PdfPTable(1);
        tableFooter.addCell("Observaciones: " + medExam.getObservation() + "\n" + "Resultado: " + medExam.getResult() + "\n \n");
        return tableFooter;
    }

    private PdfPTable generateTableContent(Long exam) {

        PdfPTable tableContent = new PdfPTable(2);
        pdfUtil.addTableTitle(tableContent, new String[]{"Nombre", "Valor"});
        addRows(tableContent, exam);
        return tableContent;
    }

    private void addRows(PdfPTable table, Long exam) {

        if (null != table && null != exam) {
            List<ExamDetailDto> examDetailDtos = getAllExamDetailByIdExam(exam);
            for (ExamDetailDto ex : examDetailDtos) {
                table.addCell(ex.getName());
                table.addCell(ex.getValue());
            }
        }
    }

}
