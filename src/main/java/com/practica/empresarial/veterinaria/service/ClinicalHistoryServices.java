package com.practica.empresarial.veterinaria.service;

import com.practica.empresarial.veterinaria.exceptions.ClinicalHistoryException;
import com.practica.empresarial.veterinaria.model.ClinicalHistory;
import com.practica.empresarial.veterinaria.model.dto.ClinicalHistoryDto;
import com.practica.empresarial.veterinaria.model.transform.ClinicalHistoryTransform;
import com.practica.empresarial.veterinaria.repository.ClinicalHistoryRepository;
import com.practica.empresarial.veterinaria.utils.MessageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClinicalHistoryServices {

    private final MessageUtil messageUtil;
    private final ClinicalHistoryRepository clinicalHistoryRepository;
    private final ClinicalHistoryTransform clinicalHistoryTransform;

    @Autowired
    public ClinicalHistoryServices(ClinicalHistoryRepository clinicalHistoryRepository,
                                   ClinicalHistoryTransform clinicalHistoryTransform, MessageUtil messageUtil) {
        this.clinicalHistoryRepository = clinicalHistoryRepository;
        this.clinicalHistoryTransform = clinicalHistoryTransform;
        this.messageUtil = messageUtil;
    }

    public ClinicalHistoryDto createClinicalHistory(ClinicalHistoryDto clinicalHistoryDto) {

        try {
            ClinicalHistory clinicalHistoryPersist = clinicalHistoryTransform.clinicalHistoryDtoToClinicalHistory(clinicalHistoryDto);
            clinicalHistoryPersist.validateCreate();
            return clinicalHistoryTransform.clinicalHistoryToClinicalHistoryDto(clinicalHistoryRepository.save(clinicalHistoryPersist));
        } catch (NullPointerException e) {
            throw new ClinicalHistoryException(messageUtil.resolveMessageSource("dto.null.exception.message"));
        }
    }
}
