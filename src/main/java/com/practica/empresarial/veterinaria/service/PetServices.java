package com.practica.empresarial.veterinaria.service;

import com.practica.empresarial.veterinaria.exceptions.PetException;
import com.practica.empresarial.veterinaria.model.dto.PetDto;
import com.practica.empresarial.veterinaria.model.transform.PetTransform;
import com.practica.empresarial.veterinaria.repository.PetRepository;
import com.practica.empresarial.veterinaria.utils.MessageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PetServices {

    private final PetRepository petRepository;
    private final MessageUtil messageUtil;
    private final PetTransform petTransform;


    @Autowired
    public PetServices(PetRepository petRepository, MessageUtil messageResolve, PetTransform petTransform) {
        this.petRepository = petRepository;
        this.messageUtil = messageResolve;
        this.petTransform = petTransform;
    }


    public List<PetDto> getAllPetByUser(String document) {
        if (null != document) {
            return petRepository.findAllByDocumentUser(document).stream()
                    .map(petTransform::petToPetDto)
                    .collect(Collectors.toList());
        }
        throw new PetException(messageUtil.resolveMessageSource("fieldRequire.exception.message"));

    }

}
