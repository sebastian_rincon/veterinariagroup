package com.practica.empresarial.veterinaria.service;

import com.practica.empresarial.veterinaria.exceptions.ClinicalHistoryDetailException;
import com.practica.empresarial.veterinaria.model.ClinicalHistoryDetail;
import com.practica.empresarial.veterinaria.model.dto.ClinicalHistoryDetailDto;
import com.practica.empresarial.veterinaria.model.transform.ClinicalHistoryDetailTransform;
import com.practica.empresarial.veterinaria.repository.ClinicalHistoryDetailRepository;
import com.practica.empresarial.veterinaria.utils.MessageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClinicalHistoryDetailServices {
    private final MessageUtil messageUtil;
    private final ClinicalHistoryDetailTransform clinicalHistoryDetailTransform;
    private final ClinicalHistoryDetailRepository clinicalHistoryDetailRepository;

    @Autowired
    public ClinicalHistoryDetailServices(ClinicalHistoryDetailTransform clinicalHistoryDetailTransform, ClinicalHistoryDetailRepository
            clinicalHistoryDetailRepository, MessageUtil messageUtil) {
        this.clinicalHistoryDetailTransform = clinicalHistoryDetailTransform;
        this.clinicalHistoryDetailRepository = clinicalHistoryDetailRepository;
        this.messageUtil = messageUtil;
    }

    public ClinicalHistoryDetailDto createClinicalHistory(ClinicalHistoryDetailDto clinicalHistory) {

        try {
            ClinicalHistoryDetail clinicalHistoryPersist = clinicalHistoryDetailTransform.clinicalHistoryDtoToClinicalHistory(clinicalHistory);
            clinicalHistoryPersist.validateCreate();
            return clinicalHistoryDetailTransform.clinicalHistoryToClinicalHistoryDto(clinicalHistoryDetailRepository.save(clinicalHistoryPersist));
        } catch (NullPointerException e) {
            throw new ClinicalHistoryDetailException(messageUtil.resolveMessageSource("dto.null.exception.message"));
        }
    }
}
