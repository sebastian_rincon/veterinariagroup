package com.practica.empresarial.veterinaria.service;

import com.practica.empresarial.veterinaria.model.dto.DetailInvoiceDto;
import com.practica.empresarial.veterinaria.model.transform.DetailInvoiceTransform;
import com.practica.empresarial.veterinaria.repository.DetailInvoiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


@Service
public class DetailInvoiceService {

    private final DetailInvoiceTransform detailInvoiceTransform;
    private final DetailInvoiceRepository detailInvoiceRepository;

    @Autowired
    public DetailInvoiceService(DetailInvoiceTransform detailInvoiceTransform, DetailInvoiceRepository detailInvoiceRepository) {
        this.detailInvoiceTransform = detailInvoiceTransform;
        this.detailInvoiceRepository = detailInvoiceRepository;
    }

    public List<DetailInvoiceDto> getAllByidInvoice(Integer idInvoice) {

        return detailInvoiceRepository.findAllByIdInvoice(idInvoice).stream()
                .map(detailInvoiceTransform::detailInvoiceToDetailInvoiceDto)
                .collect(Collectors.toList());
    }

}


