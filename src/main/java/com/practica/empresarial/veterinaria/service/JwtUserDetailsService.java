package com.practica.empresarial.veterinaria.service;

import com.practica.empresarial.veterinaria.exceptions.UserException;
import com.practica.empresarial.veterinaria.model.dto.UserVeterinaryDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class JwtUserDetailsService implements UserDetailsService {

    private final UserVeterinaryService userVeterinaryService;

    @Autowired
    public JwtUserDetailsService(UserVeterinaryService userVeterinaryService) {
        this.userVeterinaryService = userVeterinaryService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UserException {

        UserVeterinaryDto userDb = getUserByUsername(username);

        if (null == userDb) {
            throw new UsernameNotFoundException("User not found with username: " + username);
        }

        return new User(userDb.getUserName(), userDb.getPass(), new ArrayList<>());

    }

    private UserVeterinaryDto getUserByUsername(String userName) {
        return userVeterinaryService.getUserByUserName(userName);
    }
}
