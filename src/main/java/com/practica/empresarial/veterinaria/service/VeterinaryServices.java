package com.practica.empresarial.veterinaria.service;

import com.practica.empresarial.veterinaria.exceptions.VeterinaryException;
import com.practica.empresarial.veterinaria.model.Veterinary;
import com.practica.empresarial.veterinaria.model.dto.VeterinaryDto;
import com.practica.empresarial.veterinaria.model.transform.VeterinaryTransform;
import com.practica.empresarial.veterinaria.repository.VeterinaryRepository;
import com.practica.empresarial.veterinaria.utils.MessageUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


@Service
public class VeterinaryServices {

    private final MessageUtil messageUtil;
    private final VeterinaryTransform veterinaryTransform;
    private final VeterinaryRepository veterinaryRepository;

    @Autowired
    public VeterinaryServices(VeterinaryRepository veterinaryRepository, VeterinaryTransform veterinaryTransform, MessageUtil messageResolve) {
        this.veterinaryRepository = veterinaryRepository;
        this.veterinaryTransform = veterinaryTransform;
        this.messageUtil = messageResolve;
    }

    public VeterinaryDto createVeterinary(VeterinaryDto veterinary) {

        try {
            Veterinary veterinaryPersist = veterinaryTransform.veterinaryDtoToVeterinary(veterinary);
            veterinaryPersist.validateCreate();
            return veterinaryTransform.veterinaryToVeterinaryDto(veterinaryRepository.save(veterinaryPersist));
        } catch (NullPointerException e) {
            throw new VeterinaryException(messageUtil.resolveMessageSource("dto.null.exception.message"));
        }
    }

    public VeterinaryDto updateVeterinary(VeterinaryDto veterinary) {

        try {
            Veterinary veterinaryPersist = veterinaryTransform.veterinaryDtoToVeterinary(veterinary);
            veterinaryPersist.validateUpdate();
            return veterinaryTransform.veterinaryToVeterinaryDto(veterinaryRepository.save(veterinaryPersist));
        } catch (NullPointerException e) {
            throw new VeterinaryException(messageUtil.resolveMessageSource("dto.null.exception.message"));
        }
    }

    public List<VeterinaryDto> getAllVeterinary() {

        return veterinaryRepository.findAll()
                .stream()
                .map(veterinaryTransform::veterinaryToVeterinaryDto)
                .collect(Collectors.toList());
    }

    public List<VeterinaryDto> getAllVeterinaryByName(String name) {

        if (StringUtils.isNotBlank(name)) {
            return veterinaryRepository.findAllByName(name)
                    .stream()
                    .map(veterinaryTransform::veterinaryToVeterinaryDto)
                    .collect(Collectors.toList());
        }
        throw new VeterinaryException(messageUtil.resolveMessageSource("fieldRequire.exception.message"));
    }

    public String deleteVeterinary(Long idVeterinary) {

        if (null != idVeterinary) {
            veterinaryRepository.deleteById(idVeterinary);
            return messageUtil.resolveMessageSource("successful.delete.message");
        }
        throw new VeterinaryException(messageUtil.resolveMessageSource("fieldEmpty.exception.message"));
    }

}
