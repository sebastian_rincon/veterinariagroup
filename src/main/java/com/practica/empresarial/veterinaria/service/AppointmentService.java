package com.practica.empresarial.veterinaria.service;

import com.practica.empresarial.veterinaria.model.dto.AppointmentDto;
import com.practica.empresarial.veterinaria.model.transform.AppointmentTransform;
import com.practica.empresarial.veterinaria.repository.AppointmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AppointmentService {

    private final AppointmentRepository appointmentRepository;
    private final AppointmentTransform appointmentTransform;


    @Autowired
    public AppointmentService(AppointmentRepository appointmentRepository, AppointmentTransform appointmentTransform) {
        this.appointmentRepository = appointmentRepository;
        this.appointmentTransform = appointmentTransform;

    }

    public List<AppointmentDto> getAllByTopicAppointment(String topicAppointment) {
        return appointmentRepository.findAllByTopicAppointment(topicAppointment)
                .stream()
                .map(appointmentTransform::appointmentToAppointmentDto)
                .collect(Collectors.toList());
    }
}

