package com.practica.empresarial.veterinaria.controller;

import com.practica.empresarial.veterinaria.model.Category;
import com.practica.empresarial.veterinaria.model.dto.CategoryDto;
import com.practica.empresarial.veterinaria.service.CategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/category")
@Api("category")
public class CategoryController {
    final CategoryService categoryService;

    @Autowired
    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @PostMapping(path = "/save")
    @ApiOperation(value = "Create category", response = Category.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 500, message = "Internal Server Error")})
    public CategoryDto createCategory(@RequestBody CategoryDto category) {
        return categoryService.createCategory(category);
    }

    @PutMapping(path = "/update")
    @ApiOperation(value = "Update category", response = Category.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 500, message = "Internal Server Error")})
    public CategoryDto updateCategory(@RequestBody CategoryDto category) {
        return categoryService.updateCategory(category);

    }

    @GetMapping(path = "/get/name")
    @ApiOperation(value = "get category for name", response = Category.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 500, message = "Internal Server Error")})
    public ResponseEntity<CategoryDto> getOneByName(@RequestParam(name = "name") String name) {
        return ResponseEntity.ok(categoryService.getOneByName(name));
    }

}
