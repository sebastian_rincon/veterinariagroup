package com.practica.empresarial.veterinaria.controller;

import com.practica.empresarial.veterinaria.service.MedicalExamServices;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/medicalexam")
@Api("medicalExam")
public class MedicalExamController {
    private final MedicalExamServices medicalExamServices;

    @Autowired
    public MedicalExamController(MedicalExamServices medicalExamServices) {
        this.medicalExamServices = medicalExamServices;
    }

    @GetMapping(path = "/pdf/email")
    @ApiOperation(value = "send mail", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 500, message = "Internal Server Error")})
    public String sendMailPDF(@RequestParam(name = "email") String email, @RequestParam(name = "exam") Long id) {
        return medicalExamServices.sendByEmailMedicalExamReport(email, id);
    }


}
