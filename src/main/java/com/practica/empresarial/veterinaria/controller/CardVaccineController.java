package com.practica.empresarial.veterinaria.controller;

import com.practica.empresarial.veterinaria.model.DetailVaccine;
import com.practica.empresarial.veterinaria.model.dto.CardVaccineDto;
import com.practica.empresarial.veterinaria.service.CardVaccineServices;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/cardvaccine")
@Api("cardvaccine")
public class CardVaccineController {

    private final CardVaccineServices cardVaccineServices;

    @Autowired
    public CardVaccineController(CardVaccineServices cardVaccineServices) {
        this.cardVaccineServices = cardVaccineServices;
    }

    @GetMapping(path = "/consul")
    @ApiOperation(value = "get", response = DetailVaccine.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 500, message = "Internal Server Error")})
    public CardVaccineDto getOneByIdPet(@RequestParam("idpet") Integer idPet) {
        return cardVaccineServices.getOneByIdPet(idPet);
    }

    @GetMapping(path = "/info")
    @ApiOperation(value = "get", response = DetailVaccine.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 500, message = "Internal Server Error")})
    public Map<String, Object> getCardVaccine(@RequestParam("idpet") Integer idPet) {
        return cardVaccineServices.getCardVaccine(idPet);
    }

    @GetMapping(path = "/pdf/email")
    @ApiOperation(value = "send mail", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 500, message = "Internal Server Error")})
    public String sendMailPDF(@RequestParam(name = "email") String email, @RequestParam(name = "idPet") Integer idPet) {
        return cardVaccineServices.sendByEmailCardVaccine(email, idPet);
    }

}
