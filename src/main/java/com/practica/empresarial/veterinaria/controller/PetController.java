package com.practica.empresarial.veterinaria.controller;

import com.practica.empresarial.veterinaria.model.dto.PetDto;
import com.practica.empresarial.veterinaria.service.PetServices;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/pet")
@Api("pet")
public class PetController {
    final PetServices petServices;

    @Autowired
    public PetController(PetServices petServices) {
        this.petServices = petServices;
    }

    @GetMapping(path = "/pets/user")
    @ApiOperation(value = "get pets for user", response = PetDto.class)
    public List<PetDto> getAllPetByUser(@RequestParam("identificacion") String document) {
        return petServices.getAllPetByUser(document);
    }

}
