package com.practica.empresarial.veterinaria.controller;

import com.practica.empresarial.veterinaria.model.dto.LoginRequest;
import com.practica.empresarial.veterinaria.model.dto.SuccessLoginDto;
import com.practica.empresarial.veterinaria.model.dto.UserVeterinaryDto;
import com.practica.empresarial.veterinaria.service.AuthService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/auth")
@Api("Authentication")
public class AuthController {

    private final AuthService authService;

    @Autowired
    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    @PostMapping(value = "/login")
    @ApiOperation(value = "Authenticate user", response = SuccessLoginDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 500, message = "Internal Server Error")})
    public ResponseEntity<SuccessLoginDto> login(@RequestBody LoginRequest login) {
        return ResponseEntity.ok(authService.generateAuthenticationToken(login));
    }

    @PostMapping(value = "/register")
    @ApiOperation(value = "Register user", response = UserVeterinaryDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 500, message = "Internal Server Error")})
    public ResponseEntity<UserVeterinaryDto> registerUser(@RequestBody UserVeterinaryDto user) {
        return ResponseEntity.ok(authService.registerUser(user));
    }
}
