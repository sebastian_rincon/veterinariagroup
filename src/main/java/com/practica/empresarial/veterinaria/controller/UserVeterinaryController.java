package com.practica.empresarial.veterinaria.controller;

import com.practica.empresarial.veterinaria.service.UserVeterinaryService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
@Api("user")
public class UserVeterinaryController {

    @Autowired
    private UserVeterinaryService userVeterinaryService;

    @Autowired
    public UserVeterinaryController(UserVeterinaryService userVeterinaryService) {
        this.userVeterinaryService = userVeterinaryService;
    }

}
