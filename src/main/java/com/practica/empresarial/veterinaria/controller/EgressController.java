package com.practica.empresarial.veterinaria.controller;

import com.practica.empresarial.veterinaria.model.Egress;
import com.practica.empresarial.veterinaria.model.dto.EgressDto;
import com.practica.empresarial.veterinaria.service.EgressServices;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/egress")
@Api("egress")
public class EgressController {
    final EgressServices egressServices;

    @Autowired
    public EgressController(EgressServices egressServices) {
        this.egressServices = egressServices;
    }

    @GetMapping(path = "/all")
    @ApiOperation(value = "get all Egress ", response = Egress.class)
    public List<EgressDto> getAllEgress() {
        return egressServices.getAllEgress();
    }

    @GetMapping(path = "/all/type")
    @ApiOperation(value = "get all Egress for type", response = Egress.class)
    public List<EgressDto> getAllEgressByType(@RequestParam("type") String type) {
        return egressServices.getAllEgressByType(type);
    }

    @GetMapping(path = "/all/pet")
    @ApiOperation(value = "get all Egress for type", response = Egress.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 500, message = "Internal Server Error")})
    public List<EgressDto> getAllEgressByPet(@RequestParam("idpet") Long idPet) {
        return egressServices.getAllEgressByPet(idPet);
    }

    @PostMapping(path = "/save")
    @ApiOperation(value = "create Egress", response = EgressDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 500, message = "Internal Server Error")})
    public EgressDto createEgress(@RequestBody EgressDto egress) {
        return egressServices.createEgress(egress);
    }

    @PutMapping(path = "/update")
    @ApiOperation(value = "update Egress", response = EgressDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 500, message = "Internal Server Error")})
    public EgressDto updateEgress(@RequestBody EgressDto egress) {
        return egressServices.updateEgress(egress);
    }

}
