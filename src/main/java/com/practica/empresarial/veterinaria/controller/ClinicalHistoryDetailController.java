package com.practica.empresarial.veterinaria.controller;

import com.practica.empresarial.veterinaria.model.dto.ClinicalHistoryDetailDto;
import com.practica.empresarial.veterinaria.service.ClinicalHistoryDetailServices;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/clinicalhistorydetail")
@Api("clinicalhistorydetail")
public class ClinicalHistoryDetailController {
    final ClinicalHistoryDetailServices clinicalHistoryDetailServices;

    @Autowired
    public ClinicalHistoryDetailController(ClinicalHistoryDetailServices clinicalHistoryDetailServices) {
        this.clinicalHistoryDetailServices = clinicalHistoryDetailServices;
    }

    @PostMapping(path = "/save")
    @ApiOperation(value = "create clinicalHistory", response = ClinicalHistoryDetailDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 500, message = "Internal Server Error")})
    public ClinicalHistoryDetailDto createClinicalHistory(@RequestBody ClinicalHistoryDetailDto clinicalHistoryDetailDto) {
        return clinicalHistoryDetailServices.createClinicalHistory(clinicalHistoryDetailDto);
    }
}
