package com.practica.empresarial.veterinaria.exceptions;


public class CardVaccineException extends RuntimeException {

    public CardVaccineException(String message) {
        super(message);
    }
}
