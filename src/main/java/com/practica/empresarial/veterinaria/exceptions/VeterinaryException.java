package com.practica.empresarial.veterinaria.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)

public class VeterinaryException extends RuntimeException {

    public VeterinaryException(String message) {
        super(message);
    }

}
