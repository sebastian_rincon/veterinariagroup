package com.practica.empresarial.veterinaria.exceptions;

public class EgressException extends RuntimeException {

    public EgressException(String message) {
        super(message);
    }

}
