package com.practica.empresarial.veterinaria.exceptions;

public class CategoryException extends RuntimeException {

    public CategoryException(String message) {
        super(message);
    }
}
