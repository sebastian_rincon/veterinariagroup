package com.practica.empresarial.veterinaria.model.transform;

import com.practica.empresarial.veterinaria.exceptions.InvoiceException;
import com.practica.empresarial.veterinaria.model.Invoice;
import com.practica.empresarial.veterinaria.model.dto.InvoiceDto;
import org.springframework.stereotype.Component;

@Component
public class InvoiceTransform {
    public Invoice invoiceDtoToInvoice(InvoiceDto invoice) {
        if (invoice != null) {
            return Invoice.builder()
                    .idInvoice(invoice.getIdInvoice())
                    .dateInvoice(invoice.getDateInvoice())
                    .value(invoice.getValue())
                    .discount(invoice.getDiscount())
                    .totalPrice(invoice.getTotalPrice())
                    .idUser(invoice.getIdUser())
                    .idBranch(invoice.getIdBranch())
                    .idPet(invoice.getIdPet())
                    .build();
        }
        throw new InvoiceException("Error al construir.");
    }

    public InvoiceDto invoiceToInvoiceDto(Invoice invoice) {
        InvoiceDto newInvoiceDto = null;
        if (invoice != null) {
            newInvoiceDto = InvoiceDto.builder()
                    .idInvoice(invoice.getIdInvoice())
                    .dateInvoice(invoice.getDateInvoice())
                    .value(invoice.getValue())
                    .discount(invoice.getDiscount())
                    .totalPrice(invoice.getTotalPrice())
                    .idUser(invoice.getIdUser())
                    .idBranch(invoice.getIdBranch())
                    .idPet(invoice.getIdPet())
                    .build();
        }
        return newInvoiceDto;
    }
}
