package com.practica.empresarial.veterinaria.model;

import com.practica.empresarial.veterinaria.exceptions.CategoryException;
import com.practica.empresarial.veterinaria.model.validation.ValidationEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;

@Builder
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "categoria")
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_categoria")
    private Long idCategory;

    @Column(name = "nombre_categoria")
    private String name;

    @Column(name = "estado_categoria")
    private Boolean status;

    @Column(name = "descripcion_categoria")
    private String description;


    public void validateCategoryCreate() {
        if (this.idCategory != null) {
            throw new CategoryException("campo id erroneo");
        }
        validate();
    }

    public void validateCategoryUpdate() {
        if (null == idCategory) {

            throw new CategoryException("campo id erroneo");
        }
        validate();
    }

    private void validate() {

        validateFields();

        validateCharacters();

    }

    private void validateFields() {

        String fieldsEmpty = "los campos están vacios";

        if (StringUtils.isBlank(description)) {
            throw new CategoryException(fieldsEmpty);
        }
        if (null == status) {
            throw new CategoryException(fieldsEmpty);
        }
        if (StringUtils.isBlank(name)) {
            throw new CategoryException(fieldsEmpty);
        }
    }

    private void validateCharacters() {
        String valueInvalid = "el valor ingresado no esta permitido";
        if (StringUtils.containsAny(name, ValidationEnum.CHARTERS_SPECIALS.getValue()))
            throw new CategoryException(valueInvalid);
        if (StringUtils.containsAny(name, ValidationEnum.CHARTERS_NUMERICS.getValue()))
            throw new CategoryException(valueInvalid);
    }
}