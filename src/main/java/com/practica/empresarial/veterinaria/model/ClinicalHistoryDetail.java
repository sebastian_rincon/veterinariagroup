package com.practica.empresarial.veterinaria.model;


import com.practica.empresarial.veterinaria.exceptions.ClinicalHistoryDetailException;
import io.micrometer.core.instrument.util.StringUtils;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "detalle_historia_clinica")
public class ClinicalHistoryDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_detalle_historia_clinica")
    private Long idDetailHC;

    @Column(name = "temperatura")
    private Float temperature;

    @Column(name = "peso")
    private Float weight;

    @Column(name = "frecuencia_cardiaca")
    private Float heartRate;

    @Column(name = "frecuencia_respiratoria")
    private Float breathingFrequency;

    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
    @Column(name = "fecha")
    private Date admissionDate;

    @Column(name = "id_historia_clinica")
    private Long idClinicalHistory;

    @Column(name = "alimentacion")
    private String feeding;

    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
    @Column(name = "ultima_desparacitacion")
    private Date dewormingDate;

    @Column(name = "habitad")
    private String habitat;

    @Column(name = "tiempo_llenado_capilar")
    private Float capillaryTime;

    @Column(name = "estado_reproductivo")
    private String reproductiveStatus;

    @Column(name = "observacion")
    private String observations;

    @Column(name = "id_colaborador")
    private Long idCollaborator;

    public void validateCreate() {

        if (idDetailHC != null) {
            throw new ClinicalHistoryDetailException("campo id erroneo");
        }
        validateField();
    }

    private void validateField() {
        validateFieldNotEmpty();

        validateValueField();
    }

    private void validateFieldNotEmpty() {

        String fieldsEmpty = "los campos están vacios";
        if (temperature == null) {
            throw new ClinicalHistoryDetailException(fieldsEmpty);
        }
        if (weight == null) {
            throw new ClinicalHistoryDetailException(fieldsEmpty);
        }
        if (heartRate == null) {
            throw new ClinicalHistoryDetailException(fieldsEmpty);
        }
        if (breathingFrequency == null) {
            throw new ClinicalHistoryDetailException(fieldsEmpty);
        }
        if (admissionDate == null) {
            throw new ClinicalHistoryDetailException(fieldsEmpty);
        }
        if (idClinicalHistory == null) {
            throw new ClinicalHistoryDetailException(fieldsEmpty);
        }
        if (StringUtils.isBlank(feeding)) {
            throw new ClinicalHistoryDetailException(fieldsEmpty);
        }
        if (dewormingDate == null) {
            throw new ClinicalHistoryDetailException(fieldsEmpty);
        }
        if (StringUtils.isBlank(habitat)) {
            throw new ClinicalHistoryDetailException(fieldsEmpty);
        }
        if (capillaryTime == null) {
            throw new ClinicalHistoryDetailException(fieldsEmpty);
        }
        if (StringUtils.isBlank(reproductiveStatus)) {
            throw new ClinicalHistoryDetailException(fieldsEmpty);
        }
        if (StringUtils.isBlank(observations)) {
            throw new ClinicalHistoryDetailException(fieldsEmpty);
        }
        if (idCollaborator == null) {
            throw new ClinicalHistoryDetailException(fieldsEmpty);
        }
    }

    private void validateValueField() {

        String valueInvalid = "el valor ingresado no esta permitido";
        if (temperature <= 0f) {
            throw new ClinicalHistoryDetailException(valueInvalid);
        }
        if (weight <= 0f) {
            throw new ClinicalHistoryDetailException(valueInvalid);
        }
        if (heartRate <= 0f) {
            throw new ClinicalHistoryDetailException(valueInvalid);
        }
        if (breathingFrequency <= 0f) {
            throw new ClinicalHistoryDetailException(valueInvalid);
        }
        if (idClinicalHistory <= 0L) {
            throw new ClinicalHistoryDetailException(valueInvalid);
        }
        if (capillaryTime <= 0f) {
            throw new ClinicalHistoryDetailException(valueInvalid);
        }
        if (idCollaborator <= 0L) {
            throw new ClinicalHistoryDetailException(valueInvalid);
        }
    }
}
