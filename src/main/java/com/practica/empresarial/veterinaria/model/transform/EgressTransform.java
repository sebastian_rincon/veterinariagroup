package com.practica.empresarial.veterinaria.model.transform;

import com.practica.empresarial.veterinaria.exceptions.EgressException;
import com.practica.empresarial.veterinaria.model.Egress;
import com.practica.empresarial.veterinaria.model.dto.EgressDto;
import org.springframework.stereotype.Component;

@Component
public class EgressTransform {


    public Egress egressDtoToEgress(EgressDto egress) {
        if (egress != null) {
            return Egress.builder()
                    .idEgress(egress.getIdEgress())
                    .concept(egress.getConcept())
                    .description(egress.getDescription())
                    .date(egress.getDate())
                    .type(egress.getType())
                    .value(egress.getValue())
                    .idPet(egress.getIdPet())
                    .idUser(egress.getIdUser())
                    .build();
        }
        throw new EgressException("Error al construir.");
    }

    public EgressDto egressToEgressDto(Egress egress) {
        EgressDto newEgressDto = null;
        if (egress != null) {
            newEgressDto = EgressDto.builder()
                    .idEgress(egress.getIdEgress())
                    .concept(egress.getConcept())
                    .description(egress.getDescription())
                    .date(egress.getDate())
                    .type(egress.getType())
                    .value(egress.getValue())
                    .idUser(egress.getIdUser())
                    .idPet(egress.getIdPet())
                    .build();

        }
        return newEgressDto;
    }
}
