package com.practica.empresarial.veterinaria.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "examen_medico", schema = "public")
public class MedicalExam {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_examen_medico")
    private Long idMedicalExam;

    @Column(name = "id_tipo_examen")
    private Long type;

    @Column(name = "id_mascota")
    private Long pet;

    @Column(name = "observacion_examen")
    private String observation;

    @Column(name = "resultado_examen")
    private String result;

}
