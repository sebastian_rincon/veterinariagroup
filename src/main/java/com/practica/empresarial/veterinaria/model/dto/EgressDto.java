package com.practica.empresarial.veterinaria.model.dto;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Builder
@Data
public class EgressDto {

    private Long idEgress;
    private Date date;
    private String type;
    private Float value;
    private String concept;
    private String description;
    private Long idUser;
    private Long idPet;
}
