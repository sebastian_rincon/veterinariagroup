package com.practica.empresarial.veterinaria.model.dto;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class CardVaccineDto {

    private Long idCardVaccine;
    private Integer idPet;
}
