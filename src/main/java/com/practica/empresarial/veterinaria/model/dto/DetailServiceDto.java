package com.practica.empresarial.veterinaria.model.dto;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class DetailServiceDto {

    private Long idDetail;
    private Long quantity;
    private Long price;
    private Long idService;
    private Long idInvoice;
    private Long idCollaborator;

}
