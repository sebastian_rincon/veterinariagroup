package com.practica.empresarial.veterinaria.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "detalle_examen", schema = "public")
public class ExamDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_detalle_examen")
    private Long idDetailExam;

    @Column(name = "nombre_detalle_examen")
    private String name;

    @Column(name = "valor_detalle_examen")
    private String value;

    @Column(name = "id_examen_medico")
    private Long idExam;


}
