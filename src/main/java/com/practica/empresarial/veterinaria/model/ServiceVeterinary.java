package com.practica.empresarial.veterinaria.model;

import com.practica.empresarial.veterinaria.exceptions.ServiceVeterinaryException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;

@Builder
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "servicio")
public class ServiceVeterinary {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_servicio")
    private Long idService;

    @Column(name = "nombre_servicio")
    private String nameService;

    @Column(name = "cantidad_servicio")
    private Long quantityService;

    @Column(name = "precio_servicio")
    private Float valueService;

    @Column(name = "descripcion_servicio")
    private String descriptionService;

    @Column(name = "estado_servicio")
    private Boolean statusService;


    public void validateCreate() {
        if (this.idService != null) {
            throw new ServiceVeterinaryException("campo id erroneo");
        }
        validate();
    }

    public void validateUpdate() {
        if (null == idService) {
            throw new ServiceVeterinaryException("campo id erroneo");
        }
        validate();
    }

    private void validate() {

        validateField();

        validateValueField();

    }

    private void validateField() {

        String fieldsEmpty = "los campos están vacios";
        if (StringUtils.isBlank(nameService)) {
            throw new ServiceVeterinaryException(fieldsEmpty);
        }
        if (StringUtils.isBlank(descriptionService)) {
            throw new ServiceVeterinaryException(fieldsEmpty);
        }
        if (null == valueService) {
            throw new ServiceVeterinaryException(fieldsEmpty);
        }
        if (null == quantityService) {
            throw new ServiceVeterinaryException(fieldsEmpty);
        }
        if (null == statusService) {
            throw new ServiceVeterinaryException(fieldsEmpty);
        }
    }

    private void validateValueField() {

        String valueInvalid = "el valor ingresado no esta permitido";
        if (valueService <= 0f) {
            throw new ServiceVeterinaryException(valueInvalid);
        }
    }
}
