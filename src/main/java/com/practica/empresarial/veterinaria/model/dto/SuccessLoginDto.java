package com.practica.empresarial.veterinaria.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class SuccessLoginDto {
    private String token;
}
