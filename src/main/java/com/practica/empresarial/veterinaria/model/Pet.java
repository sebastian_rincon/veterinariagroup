package com.practica.empresarial.veterinaria.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Builder
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "mascota")
public class Pet {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_mascota")
    private Long idPet;

    @Column(name = "nombre_mascota")
    private String name;

    @Column(name = "especie_mascota")
    private String species;

    @Column(name = "edad_mascota")
    private Integer age;

    @Column(name = "raza_mascota")
    private String race;

    @Column(name = "sexo_mascota")
    private String sex;

    @Column(name = "observacion_mascota")
    private String observations;

    @Column(name = "id_usuario")
    private Integer idUser;

    @Column(name = "estado_mascota")
    private Boolean status;

}
