package com.practica.empresarial.veterinaria.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Builder
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "carne_vacuna")
public class CardVaccine {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_carne_vacuna")
    private Long idCardVaccine;

    @Column(name = "id_mascota")
    private Integer idPet;

}
