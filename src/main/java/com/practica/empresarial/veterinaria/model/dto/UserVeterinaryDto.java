package com.practica.empresarial.veterinaria.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Builder
@Data
public class UserVeterinaryDto implements Serializable {

    private static final long serialVersionUID = 4854270497216639286L;
    private Long idUser;
    private String name;
    private String lastName;
    private String documentType;
    private String documentNumber;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date birthDate;
    private String department;
    private String city;
    private String neighborhood;
    private Long phone;
    private String email;
    private String userName;
    private String pass;
    private Boolean status;
    private Integer rol;
    private String token;
}
