package com.practica.empresarial.veterinaria.model.dto;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class ServiceVeterinaryDto {

    private Long idService;
    private String nameService;
    private Long quantityService;
    private Float valueService;
    private String descriptionService;
    private Boolean statusService;
}
