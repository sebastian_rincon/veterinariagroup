package com.practica.empresarial.veterinaria.model.transform;

import com.practica.empresarial.veterinaria.model.Pet;
import com.practica.empresarial.veterinaria.model.dto.PetDto;
import org.springframework.stereotype.Component;

@Component
public class PetTransform {


    public PetDto petToPetDto(Pet pet) {
        PetDto petDto = null;
        if (pet != null) {
            petDto = PetDto.builder()
                    .idPet(pet.getIdPet())
                    .name(pet.getName())
                    .species(pet.getSpecies())
                    .age(pet.getAge())
                    .race(pet.getRace())
                    .sex(pet.getSex())
                    .observations(pet.getObservations())
                    .idUser(pet.getIdUser())
                    .status(pet.getStatus())
                    .build();
        }
        return petDto;
    }
}
