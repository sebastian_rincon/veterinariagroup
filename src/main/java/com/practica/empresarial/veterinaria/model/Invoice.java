package com.practica.empresarial.veterinaria.model;

import com.practica.empresarial.veterinaria.exceptions.InvoiceException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "factura")
public class Invoice {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_factura")
    private Long idInvoice;

    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
    @Column(name = "fecha_factura")
    private Date dateInvoice;

    @Column(name = "valor_bruto")
    private Float value;

    @Column(name = "descuento")
    private Float discount;

    @Column(name = "precio_total")
    private Float totalPrice;

    @Column(name = "id_usuario")
    private Integer idUser;

    @Column(name = "id_sucursal")
    private Integer idBranch;

    @Column(name = "id_mascota")
    private Integer idPet;

    @Column(name = "status")
    private Boolean status;

    public void validateUpdate() {
        if (null == this.discount) {
            throw new InvoiceException("campo id erroneo");
        }
        validateField();
    }

    private void validateField() {

        validateFieldNotEmpty();
        validateValueField();
    }

    private void validateFieldNotEmpty() {

        String fieldsEmpty = "los campos están vacios";
        if (null == (discount)) {
            throw new InvoiceException(fieldsEmpty);
        }
        if (null == dateInvoice) {
            throw new InvoiceException(fieldsEmpty);
        }
        if (null == totalPrice) {
            throw new InvoiceException(fieldsEmpty);
        }
    }

    private void validateValueField() {

        String valueInvalid = "el valor ingresado no esta permitido";
        if (totalPrice <= 0f) {
            throw new InvoiceException(valueInvalid);
        }
    }

}

