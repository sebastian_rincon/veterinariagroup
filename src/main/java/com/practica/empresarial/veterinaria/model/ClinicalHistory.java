package com.practica.empresarial.veterinaria.model;

import com.practica.empresarial.veterinaria.exceptions.ClinicalHistoryException;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "historia_clinica")
public class ClinicalHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_historia_clinica")
    private Long idClinicalHistory;

    @Column(name = "id_mascota")
    private Long idPet;

    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
    @Column(name = "fecha_creacion")
    private Date dateCreate;

    public void validateCreate() {
        if (this.idClinicalHistory != null) {
            throw new ClinicalHistoryException("campo id erroneo");
        }
        validateField();
    }

    private void validateField() {
        validateFieldNoEmpty();

        validateValueField();
    }

    private void validateFieldNoEmpty() {
        String fieldsEmpty = "los campos están vacíos";
        if (dateCreate == null) {
            throw new ClinicalHistoryException(fieldsEmpty);
        }
        if (idPet == null) {
            throw new ClinicalHistoryException(fieldsEmpty);
        }
    }

    private void validateValueField() {
        String valueInvalid = "El valor ingresado no está permitido";
        if (idPet <= 0f) {
            throw new ClinicalHistoryException(valueInvalid);
        }
    }
}

