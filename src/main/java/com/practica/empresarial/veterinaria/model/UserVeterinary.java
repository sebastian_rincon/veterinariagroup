package com.practica.empresarial.veterinaria.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "usuario")
public class UserVeterinary implements Serializable {

    private static final long serialVersionUID = -4252714288709363360L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_usuario")
    private Long idUser;

    @Column(name = "nombre_usuario")
    private String name;

    @Column(name = "apellido_usuario")
    private String lastName;

    @Column(name = "tipo_documento_usuario")
    private String documentType;

    @Column(name = "documento_usuario")
    private String documentNumber;

    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
    @Column(name = "fecha_nacimiento_usuario")
    private Date birthDate;

    @Column(name = "departamento_residencia_usuario")
    private String department;

    @Column(name = "ciudad_residencia_usuario")
    private String city;

    @Column(name = "barrio_residencia_usuario")
    private String neighborhood;

    @Column(name = "telefono_usuario")
    private Long phone;

    @Column(name = "correo_usuario")
    private String email;

    @Column(name = "user_name_usuario")
    private String userName;

    @Column(name = "password_usuario")
    private String pass;

    @Column(name = "estado_usuario")
    private Boolean status;

    @Column(name = "rolid_rol")
    private Integer rol;

    @Column(name = "token")
    private String token;
}
