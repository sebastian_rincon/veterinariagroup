package com.practica.empresarial.veterinaria.model.transform;

import com.practica.empresarial.veterinaria.model.DetailService;
import com.practica.empresarial.veterinaria.model.dto.DetailServiceDto;
import org.springframework.stereotype.Component;

@Component
public class DetailServiceTransform {

    public DetailService detailServiceDtoToServiceDetail(DetailServiceDto detailServiceDto) {
        DetailService detailService = null;
        if (null != detailServiceDto) {
            detailService = DetailService.builder()
                    .idDetail(detailServiceDto.getIdDetail())
                    .quantity(detailServiceDto.getQuantity())
                    .price(detailServiceDto.getPrice())
                    .idService(detailServiceDto.getIdService())
                    .idInvoice(detailServiceDto.getIdInvoice())
                    .idCollaborator(detailServiceDto.getIdCollaborator())
                    .build();
        }
        return detailService;

    }


    public DetailServiceDto detailServiceToDetailServiceDto(DetailService detailService) {
        DetailServiceDto detailServiceDto = null;
        if (null != detailService) {
            detailServiceDto = DetailServiceDto.builder()
                    .idDetail(detailService.getIdDetail())
                    .quantity(detailService.getQuantity())
                    .price(detailService.getPrice())
                    .idService(detailService.getIdService())
                    .idInvoice(detailService.getIdInvoice())
                    .idCollaborator(detailService.getIdCollaborator())
                    .build();
        }
        return detailServiceDto;
    }
}
