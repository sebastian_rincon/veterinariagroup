package com.practica.empresarial.veterinaria.model.transform;

import com.practica.empresarial.veterinaria.model.ClinicalHistory;
import com.practica.empresarial.veterinaria.model.dto.ClinicalHistoryDto;
import org.springframework.stereotype.Component;

@Component
public class ClinicalHistoryTransform {

    public ClinicalHistory clinicalHistoryDtoToClinicalHistory(ClinicalHistoryDto clinicalHistoryDto) {
        ClinicalHistory clinicalHistory = null;
        if (clinicalHistoryDto != null) {
            clinicalHistory = ClinicalHistory.builder()
                    .idClinicalHistory(clinicalHistoryDto.getIdClinicalHistory())
                    .dateCreate(clinicalHistoryDto.getDateCreate())
                    .idPet(clinicalHistoryDto.getIdPet())
                    .build();
        }
        return clinicalHistory;
    }

    public ClinicalHistoryDto clinicalHistoryToClinicalHistoryDto(ClinicalHistory clinicalHistory) {
        ClinicalHistoryDto clinicalHistoryDto = null;
        if (clinicalHistory != null) {
            clinicalHistoryDto = ClinicalHistoryDto.builder()
                    .idClinicalHistory(clinicalHistory.getIdClinicalHistory())
                    .dateCreate(clinicalHistory.getDateCreate())
                    .idPet(clinicalHistory.getIdPet())
                    .build();
        }
        return clinicalHistoryDto;
    }
}
