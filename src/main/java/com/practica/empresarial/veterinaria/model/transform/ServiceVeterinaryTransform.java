package com.practica.empresarial.veterinaria.model.transform;

import com.practica.empresarial.veterinaria.exceptions.ServiceVeterinaryException;
import com.practica.empresarial.veterinaria.model.ServiceVeterinary;
import com.practica.empresarial.veterinaria.model.dto.ServiceVeterinaryDto;
import org.springframework.stereotype.Component;

@Component
public class ServiceVeterinaryTransform {

    public ServiceVeterinary serviceVeterinaryDtoToServiceVeterinary(ServiceVeterinaryDto serviceVeterinary) {
        ServiceVeterinary servicePersist;
        if (serviceVeterinary != null) {
            servicePersist = ServiceVeterinary.builder()
                    .idService(serviceVeterinary.getIdService())
                    .nameService(serviceVeterinary.getNameService())
                    .descriptionService(serviceVeterinary.getDescriptionService())
                    .quantityService(serviceVeterinary.getQuantityService())
                    .statusService(serviceVeterinary.getStatusService())
                    .valueService(serviceVeterinary.getValueService())
                    .build();

            return servicePersist;
        }

        throw new ServiceVeterinaryException("Error al construir.");
    }

    public ServiceVeterinaryDto serviceVeterinaryToServiceVeteinaryDto(ServiceVeterinary service) {
        ServiceVeterinaryDto newServiceVeterinaryDto = null;
        if (null != service) {
            newServiceVeterinaryDto = ServiceVeterinaryDto.builder()
                    .idService(service.getIdService())
                    .nameService(service.getNameService())
                    .descriptionService(service.getDescriptionService())
                    .quantityService(service.getQuantityService())
                    .statusService(service.getStatusService())
                    .valueService(service.getValueService())
                    .build();
        }
        return newServiceVeterinaryDto;
    }
}
