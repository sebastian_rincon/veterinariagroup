package com.practica.empresarial.veterinaria.model.dto;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Builder
@Data
public class IncomeDto {

    private Long idIncome;
    private Date date;
    private String type;
    private String concept;
    private Long detailInvoice;
    private Long branchOffice;
}
