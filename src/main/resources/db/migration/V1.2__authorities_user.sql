create table authorities
(
    user_name_usuario character varying(250) not null,
    authority_usuario character varying(250) not null,
    constraint fk_authorities_users foreign key (user_name_usuario) references usuario (user_name_usuario)
);

create unique index ix_auth_username on authorities (user_name_usuario, authority_usuario);