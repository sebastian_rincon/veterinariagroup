CREATE TABLE token
(
    id_token             SERIAL       NOT NULL,
    token                varchar(255) NOT NULL,
    token_refresh        varchar(255) NOT NULL,
    token_expire         date         NOT NULL,
    token_refresh_expire date         NOT NULL,
    id_usuario           int4         NOT NULL,
    PRIMARY KEY (id_token)
);

ALTER TABLE token
    ADD CONSTRAINT fk_usuario_token FOREIGN KEY (id_usuario) REFERENCES usuario (id_usuario);

ALTER TABLE token
    OWNER to veterinaria;
