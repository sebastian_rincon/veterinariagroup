CREATE TABLE carne_vacuna
(
    id_carne_vacuna serial  NOT NULL,
    id_mascota      integer NOT NULL,
    PRIMARY KEY (id_carne_vacuna)
);

CREATE TABLE categoria
(
    id_categoria          serial                 NOT NULL,
    nombre_categoria      character varying(250) NOT NULL,
    estado_categoria      boolean                NOT NULL,
    descripcion_categoria character varying(250) NOT NULL,
    PRIMARY KEY (id_categoria)
);

CREATE TABLE categoria_servicio
(
    id_categoria serial  NOT NULL,
    id_servicio  integer NOT NULL,
    PRIMARY KEY (id_categoria,
                 id_servicio)
);

CREATE TABLE cita
(
    id_cita         serial                 NOT NULL,
    fecha_hora_cita timestamp              NOT NULL,
    estado_cita     boolean                NOT NULL,
    tema_cita       character varying(250) NOT NULL,
    id_mascota      integer                NOT NULL,
    PRIMARY KEY (id_cita)
);

CREATE TABLE colaborador
(
    id_colaborador serial  NOT NULL,
    estado         boolean NOT NULL,
    id_usuario     integer NOT NULL,
    id_sucursal    integer NOT NULL,
    PRIMARY KEY (id_colaborador)
);

CREATE TABLE detalle_examen
(
    id_detalle_examen     serial                 NOT NULL,
    nombre_detalle_examen character varying(250) NOT NULL,
    valor_detalle_examen  numeric(12, 4)         NOT NULL,
    id_examen_medico      integer                NOT NULL,
    PRIMARY KEY (id_detalle_examen)
);

CREATE TABLE detalle_factura
(
    id_detalle     serial         NOT NULL,
    cantidad       integer        NOT NULL,
    precio         numeric(19, 3) NOT NULL,
    id_servicio    integer        NOT NULL,
    id_factura     integer        NOT NULL,
    id_colaborador integer        NOT NULL,
    PRIMARY KEY (id_detalle)
);

CREATE TABLE detalle_historia_clinica
(
    id_detalle_historia_clinica serial                 NOT NULL,
    temperatura                 numeric(19, 3)         NOT NULL,
    peso                        numeric(19, 3)         NOT NULL,
    frecuencia_cardiaca         numeric(19, 3)         NOT NULL,
    frecuencia_respiratoria     numeric(19, 3)         NOT NULL,
    alimentacion                character varying(250) NOT NULL,
    habitad                     character varying(250) NOT NULL,
    ultima_desparacitacion      date                   NOT NULL,
    tiempo_llenado_capilar      numeric(19, 3)         NOT NULL,
    fecha                       date                   NOT NULL,
    estado_reproductivo         character varying(250) NOT NULL,
    observacion                 character varying(250) NOT NULL,
    id_colaborador              integer                NOT NULL,
    id_historia_clinica         integer                NOT NULL,
    PRIMARY KEY (id_detalle_historia_clinica)
);

CREATE TABLE detalle_vacuna
(
    id_detalle_vacuna serial                 NOT NULL,
    nombre_vacuna     character varying(250) NOT NULL,
    dosis_vacuna      numeric(19, 3)         NOT NULL,
    fecha_vacuna      date                   NOT NULL,
    codigo_vacuna     numeric(11, 0)         NOT NULL,
    id_carne_vacuna   integer                NOT NULL,
    estado            boolean                NOT NULL,
    PRIMARY KEY (id_detalle_vacuna)
);

CREATE TABLE egreso
(
    id_egreso          serial                 NOT NULL,
    fecha_egreso       date                   NOT NULL,
    tipo_egreso        character varying(250) NOT NULL,
    valor_egreso       numeric(19, 6)         NOT NULL,
    concepto_egreso    character varying(250) NOT NULL,
    descripcion_egreso character varying(250) NOT NULL,
    id_usuario         integer                NOT NULL,
    PRIMARY KEY (id_egreso)
);

CREATE TABLE examen_medico
(
    id_examen_medico   serial                 NOT NULL,
    id_tipo_examen     integer                NOT NULL,
    id_mascota         integer                NOT NULL,
    observacion_examen character varying(250) NOT NULL,
    resultado_examen   character varying(250) NOT NULL,
    PRIMARY KEY (id_examen_medico)
);

CREATE TABLE factura
(
    id_factura    serial         NOT NULL,
    fecha_factura date           NOT NULL,
    valor_bruto   numeric(19, 3) NOT NULL,
    descuento     numeric(19, 3) NOT NULL,
    precio_total  numeric(19, 3),
    id_usuario    integer        NOT NULL,
    id_sucursal   integer        NOT NULL,
    PRIMARY KEY (id_factura)
);

CREATE TABLE historia_clinica
(
    id_historia_clinica serial  NOT NULL,
    fecha_creacion      date    NOT NULL,
    id_mascota          integer NOT NULL,
    PRIMARY KEY (id_historia_clinica)
);

CREATE TABLE ingresos
(
    id_ingreso         serial                 NOT NULL,
    fecha_ingreso      date                   NOT NULL,
    tipo_ingreso       character varying(250) NOT NULL,
    concepto_ingreso   character varying(250) NOT NULL,
    id_detalle_factura integer                NOT NULL,
    id_sucursal        integer                NOT NULL,
    PRIMARY KEY (id_ingreso)
);

CREATE TABLE mascota
(
    id_mascota          serial                 NOT NULL,
    nombre_mascota      character varying(250) NOT NULL,
    especie_mascota     character varying(250) NOT NULL,
    edad_mascota        integer,
    raza_mascota        character varying(250) NOT NULL,
    sexo_mascota        char(255)              NOT NULL,
    observacion_mascota character varying(250) NOT NULL,
    estado_mascota      boolean                NOT NULL,
    id_usuario          integer                NOT NULL,
    PRIMARY KEY (id_mascota)
);

CREATE TABLE notificación
(
    id_notificacion          serial                 NOT NULL,
    descripcion_notificacion character varying(250) NOT NULL,
    PRIMARY KEY (id_notificacion)
);
CREATE TABLE notificación_usuario
(
    id_notificacion integer NOT NULL,
    id_usuario      integer NOT NULL,
    PRIMARY KEY (id_notificacion,
                 id_usuario)
);

CREATE TABLE rol
(
    id_rol          serial                 NOT NULL,
    descripcion_rol character varying(250) NOT NULL,
    estado_rol      boolean                NOT NULL,
    PRIMARY KEY (id_rol)
);

CREATE TABLE servicio
(
    id_servicio          serial                 NOT NULL,
    nombre_servicio      character varying(250) NOT NULL,
    cantidad_servicio    integer,
    precio_servicio      numeric(19, 3)         NOT NULL,
    descripcion_servicio character varying(250) NOT NULL,
    estado_servicio      boolean                NOT NULL,
    PRIMARY KEY (id_servicio)
);

CREATE TABLE sucursal
(
    id_sucursal        serial                 NOT NULL,
    direccion_sucursal character varying(250) NOT NULL,
    telefono_sucursal  numeric(10, 0)         NOT NULL,
    logo_sucursal      character varying(250) NOT NULL,
    admin_sucursal     integer                NOT NULL,
    id_veterinaria     integer                NOT NULL,
    PRIMARY KEY (id_sucursal)
);

CREATE TABLE tipo_examen
(
    id_tipo_examen serial                 NOT NULL,
    descripcion    character varying(250) NOT NULL,
    PRIMARY KEY (id_tipo_examen)
);

CREATE TABLE usuario
(
    id_usuario                      serial                 NOT NULL,
    nombre_usuario                  character varying(250) NOT NULL,
    apellido_usuario                character varying(250) NOT NULL,
    tipo_documento_usuario          char(255)              NOT NULL,
    documento_usuario               character varying(250) NOT NULL,
    fecha_nacimiento_usuario        date                   NOT NULL,
    departamento_residencia_usuario character varying(250) NOT NULL,
    ciudad_residencia_usuario       character varying(250) NOT NULL,
    barrio_residencia_usuario       character varying(250) NOT NULL,
    telefono_usuario                numeric(12, 0)         NOT NULL,
    correo_usuario                  character varying(250) NOT NULL,
    user_name_usuario               character varying(250) NOT NULL UNIQUE,
    password_usuario                character varying(250) NOT NULL,
    estado_usuario                  boolean                NOT NULL,
    rolid_rol                       integer                NOT NULL,
    PRIMARY KEY (id_usuario)
);

CREATE TABLE veterinaria
(
    id_veterinaria     serial                 NOT NULL,
    nombre_veterinaria character varying(250) NOT NULL,
    nit_veterinaria    numeric(19, 0)         NOT NULL,
    correo_veterinaria character varying(250) NOT NULL,
    estado_vaterinaria boolean                NOT NULL,
    admin_veterinaria  integer                NOT NULL,
    PRIMARY KEY (id_veterinaria)
);



ALTER TABLE sucursal
    ADD CONSTRAINT FKsucursal568335 FOREIGN KEY (id_veterinaria) REFERENCES veterinaria (id_veterinaria);
ALTER TABLE colaborador
    ADD CONSTRAINT FKcolaborado66351 FOREIGN KEY (id_usuario) REFERENCES usuario (id_usuario);
ALTER TABLE usuario
    ADD CONSTRAINT FKusuario797381 FOREIGN KEY (rolid_rol) REFERENCES rol (id_rol);
ALTER TABLE veterinaria
    ADD CONSTRAINT FKveterinari277260 FOREIGN KEY (admin_veterinaria) REFERENCES usuario (id_usuario);
ALTER TABLE colaborador
    ADD CONSTRAINT FKcolaborado66940 FOREIGN KEY (id_sucursal) REFERENCES sucursal (id_sucursal);
ALTER TABLE notificación_usuario
    ADD CONSTRAINT FKnotificaci196106 FOREIGN KEY (id_notificacion) REFERENCES notificación (id_notificacion);
ALTER TABLE notificación_usuario
    ADD CONSTRAINT FKnotificaci638098 FOREIGN KEY (id_usuario) REFERENCES usuario (id_usuario);
ALTER TABLE egreso
    ADD CONSTRAINT FKegreso659014 FOREIGN KEY (id_usuario) REFERENCES usuario (id_usuario);
ALTER TABLE mascota
    ADD CONSTRAINT FKmascota92257 FOREIGN KEY (id_usuario) REFERENCES usuario (id_usuario);
ALTER TABLE historia_clinica
    ADD CONSTRAINT FKhistoria_c69426 FOREIGN KEY (id_mascota) REFERENCES mascota (id_mascota);
ALTER TABLE detalle_historia_clinica
    ADD CONSTRAINT FKdetalle_hi493646 FOREIGN KEY (id_historia_clinica) REFERENCES historia_clinica (id_historia_clinica);
ALTER TABLE sucursal
    ADD CONSTRAINT FKsucursal148666 FOREIGN KEY (admin_sucursal) REFERENCES usuario (id_usuario);
ALTER TABLE detalle_historia_clinica
    ADD CONSTRAINT FKdetalle_hi942860 FOREIGN KEY (id_colaborador) REFERENCES colaborador (id_colaborador);
ALTER TABLE categoria_servicio
    ADD CONSTRAINT FKcategoria_31815 FOREIGN KEY (id_categoria) REFERENCES categoria (id_categoria);
ALTER TABLE categoria_servicio
    ADD CONSTRAINT FKcategoria_278654 FOREIGN KEY (id_servicio) REFERENCES servicio (id_servicio);
ALTER TABLE factura
    ADD CONSTRAINT FKfactura732330 FOREIGN KEY (id_usuario) REFERENCES usuario (id_usuario);
ALTER TABLE detalle_factura
    ADD CONSTRAINT FKdetalle_fa106462 FOREIGN KEY (id_servicio) REFERENCES servicio (id_servicio);
ALTER TABLE detalle_factura
    ADD CONSTRAINT FKdetalle_fa627909 FOREIGN KEY (id_factura) REFERENCES factura (id_factura);
ALTER TABLE detalle_factura
    ADD CONSTRAINT FKdetalle_fa888523 FOREIGN KEY (id_colaborador) REFERENCES colaborador (id_colaborador);
ALTER TABLE factura
    ADD CONSTRAINT FKfactura400960 FOREIGN KEY (id_sucursal) REFERENCES sucursal (id_sucursal);
ALTER TABLE ingresos
    ADD CONSTRAINT FKingresos858424 FOREIGN KEY (id_detalle_factura) REFERENCES detalle_factura (id_detalle);
ALTER TABLE ingresos
    ADD CONSTRAINT FKingresos836870 FOREIGN KEY (id_sucursal) REFERENCES sucursal (id_sucursal);
ALTER TABLE carne_vacuna
    ADD CONSTRAINT FKcarne_vacu590921 FOREIGN KEY (id_mascota) REFERENCES mascota (id_mascota);
ALTER TABLE detalle_vacuna
    ADD CONSTRAINT FKdetalle_va267060 FOREIGN KEY (id_carne_vacuna) REFERENCES carne_vacuna (id_carne_vacuna);
ALTER TABLE examen_medico
    ADD CONSTRAINT FKexamen_med312746 FOREIGN KEY (id_mascota) REFERENCES mascota (id_mascota);
ALTER TABLE detalle_examen
    ADD CONSTRAINT FKdetalle_ex20239 FOREIGN KEY (id_examen_medico) REFERENCES examen_medico (id_examen_medico);
ALTER TABLE examen_medico
    ADD CONSTRAINT FKexamen_med612579 FOREIGN KEY (id_tipo_examen) REFERENCES tipo_examen (id_tipo_examen);
ALTER TABLE cita
    ADD CONSTRAINT FKcita397771 FOREIGN KEY (id_mascota) REFERENCES mascota (id_mascota);

ALTER TABLE carne_vacuna
    OWNER to veterinaria;
ALTER TABLE categoria
    OWNER to veterinaria;
ALTER TABLE categoria_servicio
    OWNER to veterinaria;
ALTER TABLE cita
    OWNER to veterinaria;
ALTER TABLE colaborador
    OWNER to veterinaria;
ALTER TABLE detalle_examen
    OWNER to veterinaria;
ALTER TABLE detalle_factura
    OWNER to veterinaria;
ALTER TABLE detalle_historia_clinica
    OWNER to veterinaria;
ALTER TABLE detalle_vacuna
    OWNER to veterinaria;
ALTER TABLE egreso
    OWNER to veterinaria;
ALTER TABLE examen_medico
    OWNER to veterinaria;
ALTER TABLE factura
    OWNER to veterinaria;
ALTER TABLE historia_clinica
    OWNER to veterinaria;
ALTER TABLE ingresos
    OWNER to veterinaria;
ALTER TABLE mascota
    OWNER to veterinaria;
ALTER TABLE notificación
    OWNER to veterinaria;
ALTER TABLE notificación_usuario
    OWNER to veterinaria;
ALTER TABLE rol
    OWNER to veterinaria;
ALTER TABLE servicio
    OWNER to veterinaria;
ALTER TABLE sucursal
    OWNER to veterinaria;
ALTER TABLE tipo_examen
    OWNER to veterinaria;
ALTER TABLE usuario
    OWNER to veterinaria;
ALTER TABLE veterinaria
    OWNER to veterinaria;

