package com.practica.empresarial.veterinaria.factorie;

import com.practica.empresarial.veterinaria.model.Egress;
import com.practica.empresarial.veterinaria.model.dto.EgressDto;

import java.util.Date;


public class EgressFactory {

    private Long idEgress;
    private Date date;
    private String type;
    private Float value;
    private String concept;
    private String description;
    private Long idUser;
    private Long idPet;

    public EgressFactory() {
        this.idEgress = 1L;
        this.date = new Date();
        this.type = "medicamento";
        this.value = 15000F;
        this.concept = "antibiotico";
        this.description = "50gr Paracetamlo";
        this.idUser = 1001L;
        this.idPet = 1001L;
    }

    public Egress newInstance() {
        return Egress.builder()
                .idEgress(this.idEgress)
                .date(this.date)
                .type(this.type)
                .value(this.value)
                .concept(this.concept)
                .description(this.description)
                .idUser(this.idUser)
                .idPet(this.idPet)
                .build();
    }

    public EgressDto newInstanceDto() {
        return EgressDto.builder()
                .idEgress(this.idEgress)
                .date(this.date)
                .type(this.type)
                .value(this.value)
                .concept(this.concept)
                .description(this.description)
                .idUser(this.idUser)
                .idPet(this.idPet)
                .build();
    }

    public EgressFactory setIdEgress(Long idEgress) {

        this.idEgress = idEgress;
        return this;
    }

    public EgressFactory setDate(Date date) {

        this.date = date;
        return this;
    }

    public EgressFactory setType(String type) {

        this.type = type;
        return this;
    }

    public EgressFactory setValue(Float value) {

        this.value = value;
        return this;
    }

    public EgressFactory setConcept(String concept) {

        this.concept = concept;
        return this;
    }

    public EgressFactory setDescription(String description) {

        this.description = description;
        return this;
    }

    public EgressFactory setIdUser(Long idUser) {

        this.idUser = idUser;
        return this;
    }

    public EgressFactory setIdPet(Long idPet) {

        this.idPet = idPet;
        return this;
    }
}
