package com.practica.empresarial.veterinaria.factorie;

import com.practica.empresarial.veterinaria.model.DetailVaccine;
import com.practica.empresarial.veterinaria.model.dto.DetailVaccineDto;

import java.util.Date;

public class DetailVaccineFactory {

    private Long idDetailVaccine;
    private String nameVaccine;
    private Float doseVaccine;
    private Date dateVaccine;
    private Float codeVaccine;
    private Integer idCardVaccine;
    private Boolean state;

    public DetailVaccineFactory() {
        this.idDetailVaccine = 1L;
        this.nameVaccine = "hepatitis";
        this.doseVaccine = 15000F;
        this.dateVaccine = new Date();
        this.codeVaccine = 15000F;
        this.idCardVaccine = 1;
        this.state = true;
    }

    public DetailVaccine newInstance() {
        return DetailVaccine.builder()
                .idDetailVaccine(this.idDetailVaccine)
                .nameVaccine(this.nameVaccine)
                .doseVaccine(this.doseVaccine)
                .dateVaccine(this.dateVaccine)
                .codeVaccine(this.codeVaccine)
                .idCardVaccine(this.idCardVaccine)
                .state(this.state)
                .build();

    }

    public DetailVaccineDto newInstanceDto() {
        return DetailVaccineDto.builder()
                .idDetailVaccine(this.idDetailVaccine)
                .nameVaccine(this.nameVaccine)
                .doseVaccine(this.doseVaccine)
                .dateVaccine(this.dateVaccine)
                .codeVaccine(this.codeVaccine)
                .idCardVaccine(this.idCardVaccine)
                .state(this.state)
                .build();

    }

    public DetailVaccineFactory setNameVaccine(String nameVaccine) {
        this.nameVaccine = nameVaccine;
        return this;
    }
}
