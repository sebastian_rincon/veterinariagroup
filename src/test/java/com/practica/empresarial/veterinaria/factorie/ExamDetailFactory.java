package com.practica.empresarial.veterinaria.factorie;

import com.practica.empresarial.veterinaria.model.ExamDetail;
import com.practica.empresarial.veterinaria.model.dto.ExamDetailDto;

public class ExamDetailFactory {
    private Long idDetailExam;
    private String name;
    private String value;
    private Long idExam;

    public ExamDetailFactory() {
        this.idDetailExam = 1L;
        this.name = "Glucosa";
        this.value = "300";
        this.idExam = 1L;
    }

    public ExamDetail newInstance() {
        return ExamDetail.builder()
                .idDetailExam(this.idDetailExam)
                .name(this.name)
                .value(this.value)
                .idExam(this.idExam)
                .build();
    }

    public ExamDetailDto newInstanceDto() {
        return ExamDetailDto.builder()
                .idDetailExam(this.idDetailExam)
                .name(this.name)
                .value(this.value)
                .idExam(this.idExam)
                .build();
    }

    public ExamDetailFactory setId(Long id) {
        this.idDetailExam = id;
        return this;
    }
}
