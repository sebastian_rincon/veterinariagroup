package com.practica.empresarial.veterinaria.factorie;

import com.practica.empresarial.veterinaria.model.CardVaccine;
import com.practica.empresarial.veterinaria.model.dto.CardVaccineDto;

public class CardVaccineFactory {

    private Long idCardVaccine;
    private Integer idPet;

    public CardVaccineFactory() {
        this.idCardVaccine = 1L;
        this.idPet = 1;
    }

    public CardVaccine newInstance() {
        return CardVaccine.builder()
                .idCardVaccine(this.idCardVaccine)
                .idPet(this.idPet)
                .build();

    }

    public CardVaccineDto newInstanceDto() {
        return CardVaccineDto.builder().build().builder()
                .idCardVaccine(this.idCardVaccine)
                .idPet(this.idPet)
                .build();

    }

    public CardVaccineFactory setIdCardVaccine(Long idCardVaccine) {
        this.idCardVaccine = idCardVaccine;
        return this;
    }
}
