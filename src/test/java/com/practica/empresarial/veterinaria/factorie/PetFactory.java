package com.practica.empresarial.veterinaria.factorie;

import com.practica.empresarial.veterinaria.model.Pet;
import com.practica.empresarial.veterinaria.model.dto.PetDto;

public class PetFactory {

    private Long idPet;
    private String name;
    private String species;
    private Integer age;
    private String race;
    private String sex;
    private String observations;
    private Integer idUser;
    private Boolean status;

    public PetFactory() {
        this.idPet = 1L;
        this.name = "Charles";
        this.species = "Perro";
        this.age = 3;
        this.race = "Doberman";
        this.sex = "M";
        this.observations = "Ninguna";
        this.idUser = 1;
        this.status = true;

    }

    public Pet newInstance() {
        return Pet.builder()
                .idPet(this.idPet)
                .name(this.name)
                .species(this.species)
                .age(this.age)
                .race(this.race)
                .sex(this.sex)
                .observations(this.observations)
                .idUser(this.idUser)
                .status(this.status)
                .build();
    }

    public PetDto newInstanceDto() {
        return PetDto.builder()
                .idPet(this.idPet)
                .name(this.name)
                .species(this.species)
                .age(this.age)
                .race(this.race)
                .sex(this.sex)
                .observations(this.observations)
                .idUser(this.idUser)
                .status(this.status)
                .build();
    }

    public PetFactory setId(Long id) {
        this.idPet = id;
        return this;
    }


}
