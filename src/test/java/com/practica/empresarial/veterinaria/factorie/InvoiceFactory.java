package com.practica.empresarial.veterinaria.factorie;

import com.practica.empresarial.veterinaria.model.Invoice;
import com.practica.empresarial.veterinaria.model.dto.InvoiceDto;

import java.util.Date;

public class InvoiceFactory {

    private Long idInvoice;
    private Date dateInvoice;
    private Float value;
    private Float discount;
    private Float totalPrice;
    private Integer idUser;
    private Integer idBranch;
    private Integer idPet;


    public InvoiceFactory() {
        this.idInvoice = 1L;
        this.dateInvoice = new Date();
        this.value = 3000f;
        this.discount = 3000f;
        this.totalPrice = 3000f;
        this.idUser = 4;
        this.idBranch = 6;
        this.idPet = 5;
    }

    public Invoice newInstance() {
        return Invoice.builder()
                .idInvoice(this.idInvoice)
                .dateInvoice(this.dateInvoice)
                .value(this.value)
                .discount(this.discount)
                .totalPrice(this.totalPrice)
                .idUser(this.idUser)
                .idBranch(this.idBranch)
                .idPet(this.idPet)
                .build();

    }

    public InvoiceDto newInstanceDto() {
        return InvoiceDto.builder()
                .idInvoice(this.idInvoice)
                .dateInvoice(this.dateInvoice)
                .value(this.value)
                .discount(this.discount)
                .totalPrice(this.totalPrice)
                .idUser(this.idUser)
                .idBranch(this.idBranch)
                .idPet(this.idPet)
                .build();
    }

    public InvoiceFactory setId(Long id) {
        this.idInvoice = id;
        return this;
    }

    public InvoiceFactory setdateInvoice(Date dateInvoice) {
        this.dateInvoice = dateInvoice;
        return this;
    }

    public InvoiceFactory setValue(Float value) {
        this.value = value;
        return this;
    }

    public InvoiceFactory setIdUser(Integer idUser) {
        this.idUser = idUser;
        return this;
    }

    public InvoiceFactory setIdBranch(Integer idBranch) {
        this.idBranch = idBranch;
        return this;
    }

    public InvoiceFactory setIdPet(Integer idPet) {
        this.idPet = idPet;
        return this;
    }

    public InvoiceFactory setIdInvoice(Long idInvoice) {
        this.idInvoice = idInvoice;
        return this;

    }

    public InvoiceFactory setDateInvoice(Date dateInvoice) {
        this.dateInvoice = dateInvoice;
        return this;
    }

    public InvoiceFactory setDiscount(Float discount) {
        this.discount = discount;
        return this;
    }

    public InvoiceFactory setTotalPrice(Float totalPrice) {
        this.totalPrice = totalPrice;
        return this;
    }
}