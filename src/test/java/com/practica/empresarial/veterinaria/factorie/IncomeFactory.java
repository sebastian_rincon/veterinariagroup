package com.practica.empresarial.veterinaria.factorie;

import com.practica.empresarial.veterinaria.model.Income;
import com.practica.empresarial.veterinaria.model.dto.IncomeDto;

import java.util.Date;

public class IncomeFactory {

    private Long idIncome;
    private Date date;
    private String type;
    private String concept;
    private Long detailInvoice;
    private Long branchOffice;

    public IncomeFactory() {
        this.idIncome = 1L;
        this.date = new Date();
        this.type = "type";
        this.concept = "concept";
        this.detailInvoice = 1L;
        this.branchOffice = 1L;

    }

    public Income newInstance() {
        return Income.builder()
                .idIncome(this.idIncome)
                .date(this.date)
                .type(this.type)
                .concept(this.concept)
                .detailInvoice(this.detailInvoice)
                .branchOffice(this.branchOffice)
                .build();
    }

    public IncomeDto newInstanceDto() {
        return IncomeDto.builder()
                .idIncome(this.idIncome)
                .date(this.date)
                .type(this.type)
                .concept(this.concept)
                .detailInvoice(this.detailInvoice)
                .branchOffice(this.branchOffice)
                .build();
    }

    public IncomeFactory setId(Long id) {
        this.idIncome = id;
        return this;
    }
}
