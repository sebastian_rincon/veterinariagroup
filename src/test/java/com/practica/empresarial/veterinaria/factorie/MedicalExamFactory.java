package com.practica.empresarial.veterinaria.factorie;

import com.practica.empresarial.veterinaria.model.MedicalExam;
import com.practica.empresarial.veterinaria.model.dto.MedicalExamDto;

public class MedicalExamFactory {

    private Long idMedicalExam;
    private Long type;
    private Long pet;
    private String observation;
    private String result;

    public MedicalExamFactory() {
        this.idMedicalExam = 1L;
        this.type = 1L;
        this.pet = 1L;
        this.observation = "observacion";
        this.result = "resultado";
    }

    public MedicalExam newInstance() {
        return MedicalExam.builder()
                .idMedicalExam(this.idMedicalExam)
                .type(this.type)
                .pet(this.pet)
                .observation(this.observation)
                .result(this.result)
                .build();
    }

    public MedicalExamDto newInstanceDto() {
        return MedicalExamDto.builder()
                .idMedicalExam(this.idMedicalExam)
                .type(this.type)
                .pet(this.pet)
                .observation(this.observation)
                .result(this.result)
                .build();
    }

    public MedicalExamFactory setId(Long id) {
        this.idMedicalExam = id;
        return this;
    }
}
