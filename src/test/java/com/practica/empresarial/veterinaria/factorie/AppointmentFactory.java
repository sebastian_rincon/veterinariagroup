package com.practica.empresarial.veterinaria.factorie;

import com.practica.empresarial.veterinaria.model.Appointment;
import com.practica.empresarial.veterinaria.model.dto.AppointmentDto;

import java.sql.Timestamp;

public class AppointmentFactory {

    private Long idAppointment;
    private Timestamp dateTimeAppointment;
    private Boolean stateAppointment;
    private String topicAppointment;
    private Integer idPet;

    public AppointmentFactory() {
        this.idAppointment = 1L;
        this.dateTimeAppointment = new Timestamp(new java.util.Date().getTime());
        this.stateAppointment = true;
        this.topicAppointment = "consulta";
        this.idPet = 1;
    }

    public Appointment newInstance() {
        return Appointment.builder()
                .idAppointment(this.idAppointment)
                .dateTimeAppointment(this.dateTimeAppointment)
                .stateAppointment(this.stateAppointment)
                .topicAppointment(this.topicAppointment)
                .idPet(this.idPet)
                .build();
    }

    public AppointmentDto newInstanceDto() {
        return AppointmentDto.builder()
                .idAppointment(this.idAppointment)
                .dateTimeAppointment(this.dateTimeAppointment)
                .stateAppointment(this.stateAppointment)
                .topicAppointment(this.topicAppointment)
                .idPet(this.idPet)
                .build();
    }

}
