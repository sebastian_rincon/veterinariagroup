package com.practica.empresarial.veterinaria.service;

import com.practica.empresarial.veterinaria.factorie.DetailVaccineFactory;
import com.practica.empresarial.veterinaria.model.DetailVaccine;
import com.practica.empresarial.veterinaria.model.dto.DetailVaccineDto;
import com.practica.empresarial.veterinaria.model.transform.DetailVaccineTransform;
import com.practica.empresarial.veterinaria.repository.DetailVaccineRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


@SpringBootTest
class DetailVaccineServicesTest {

    public static final Integer CONSULT = 1;
    public static final Integer CONSULTWO = 0;

    @Autowired
    private DetailVaccineServices detailVaccineServices;
    private DetailVaccineRepository detailVaccineRepository;

    private DetailVaccine sampleDetailVaccine;

    private List<DetailVaccine> sampleDataTest;
    private List<DetailVaccine> sampleDataTestEmpty;

    @BeforeEach
    public void setup() {
        detailVaccineRepository = mock(DetailVaccineRepository.class);
        DetailVaccineTransform detailVaccineTransform = new DetailVaccineTransform();

        detailVaccineServices = new DetailVaccineServices(detailVaccineRepository, detailVaccineTransform);
        sampleDetailVaccine = new DetailVaccineFactory().newInstance();
        sampleDataTest = new ArrayList<>();
        sampleDataTestEmpty = new ArrayList<>();
        sampleDataTest.add(sampleDetailVaccine);
    }

    @Test
    void getExistingIdCardVaccine() {
        when(detailVaccineRepository.findAllByIdCardVaccine(Mockito.anyInt())).thenReturn(sampleDataTest);
        List<DetailVaccineDto> dataConsulted = detailVaccineServices.getAllByIdCardVaccine(CONSULT);
        assertThat(dataConsulted).isNotEmpty();
    }


    @Test
    void getNoExistingIdCardVaccine() {
        when(detailVaccineRepository.findAllByIdCardVaccine(Mockito.anyInt())).thenReturn(sampleDataTestEmpty);
        List<DetailVaccineDto> dataConsulted = detailVaccineServices.getAllByIdCardVaccine(CONSULTWO);
        assertThat(dataConsulted).isEmpty();
    }

}