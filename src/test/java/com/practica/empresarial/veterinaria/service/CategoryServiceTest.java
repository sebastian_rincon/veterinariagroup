package com.practica.empresarial.veterinaria.service;

import com.practica.empresarial.veterinaria.exceptions.CategoryException;
import com.practica.empresarial.veterinaria.factorie.CategoryFactory;
import com.practica.empresarial.veterinaria.model.Category;
import com.practica.empresarial.veterinaria.model.dto.CategoryDto;
import com.practica.empresarial.veterinaria.model.transform.CategoryTransform;
import com.practica.empresarial.veterinaria.repository.CategoryRepository;
import com.practica.empresarial.veterinaria.utils.MessageUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SpringBootTest
class CategoryServiceTest {

    @Autowired
    private MessageUtil messageUtil;

    private CategoryService categoryService;
    private CategoryRepository categoryRepository;
    private Category sampleCategory;

    @BeforeEach
    public void setup() {
        categoryRepository = mock(CategoryRepository.class);
        CategoryTransform categoryTransform = new CategoryTransform();
        categoryService = new CategoryService(categoryRepository, categoryTransform, messageUtil);
        sampleCategory = new CategoryFactory().newInstance();

    }

    @Test
    void testNameInvalid() {
        when(categoryRepository.getOneByName("¨¨¨//")).thenReturn(Optional.empty());
        CategoryDto category = categoryService.getOneByName("¨¨¨//");
        assertThat(category).isNull();
    }

    @Test
    void testNameNull() {
        when(categoryRepository.getOneByName(null)).thenThrow(IllegalArgumentException.class);
        Exception exception = assertThrows(CategoryException.class, () -> categoryService.getOneByName(null));
        String expectedMessage = "el campo está vacio";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));

    }

    @Test
    void testIdFieldIsNull() {
        when(categoryRepository.save(Mockito.any(Category.class))).thenReturn(sampleCategory);
        CategoryDto categoryParam = new CategoryFactory().setIdCategory(null).newInstanceDto();
        CategoryDto category = categoryService.createCategory(categoryParam);
        assertThat(category.getIdCategory()).isNotNull();
    }

    @Test
    void testDescriptionValueFaildForNumeric() {
        when(categoryRepository.save(Mockito.any(Category.class))).thenReturn(sampleCategory);
        CategoryDto categoryParam = new CategoryFactory().setIdCategory(null).setNameCategory("limpieza1").newInstanceDto();
        Exception exception = assertThrows(CategoryException.class, () -> categoryService.createCategory(categoryParam));
        String expectedMessage = "el valor ingresado no esta permitido";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testDtoNullForCreate() {
        when(categoryRepository.save(Mockito.any(Category.class))).thenReturn(sampleCategory);
        Exception exception = assertThrows(CategoryException.class, () -> categoryService.createCategory(null));
        String expectedMessage = "Error al construir.";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testNameValueFaildForSpecialCharacters() {
        when(categoryRepository.save(Mockito.any(Category.class))).thenReturn(sampleCategory);
        CategoryDto categoryParam = new CategoryFactory().setIdCategory(null).setNameCategory("nuevo nombre/").newInstanceDto();
        Exception exception = assertThrows(CategoryException.class, () -> categoryService.createCategory(categoryParam));
        String expectedMessage = "el valor ingresado no esta permitido";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testDescriptionValueFaildForSpecialCharacters() {
        when(categoryRepository.save(Mockito.any(Category.class))).thenReturn(sampleCategory);
        CategoryDto categoryParam = new CategoryFactory().setIdCategory(null).setNameCategory("nuevo descripcion $4").newInstanceDto();
        Exception exception = assertThrows(CategoryException.class, () -> categoryService.createCategory(categoryParam));
        String expectedMessage = "el valor ingresado no esta permitido";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testNoneFieldIsEmpty() {
        when(categoryRepository.save(Mockito.any(Category.class))).thenReturn(sampleCategory);
        CategoryDto categoryParam = new CategoryFactory().setIdCategory(null).setDescriptionCategory("").newInstanceDto();
        Exception exception = assertThrows(CategoryException.class, () -> categoryService.createCategory(categoryParam));
        String expectedMessage = "los campos están vacios";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testNoneFieldNameIsEmpty() {
        when(categoryRepository.save(Mockito.any(Category.class))).thenReturn(sampleCategory);
        CategoryDto categoryParam = new CategoryFactory().setIdCategory(null).setNameCategory("").newInstanceDto();
        Exception exception = assertThrows(CategoryException.class, () -> categoryService.createCategory(categoryParam));
        String expectedMessage = "los campos están vacios";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testNoneFieldStatusIsEmpty() {
        when(categoryRepository.save(Mockito.any(Category.class))).thenReturn(sampleCategory);
        CategoryDto categoryParam = new CategoryFactory().setIdCategory(null).setStatusCategory(null).newInstanceDto();
        Exception exception = assertThrows(CategoryException.class, () -> categoryService.createCategory(categoryParam));
        String expectedMessage = "los campos están vacios";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testIdNotNullForCreate() {
        when(categoryRepository.save(Mockito.any(Category.class))).thenReturn(sampleCategory);
        CategoryDto categoryParam = new CategoryFactory().newInstanceDto();
        Exception exception = assertThrows(CategoryException.class, () -> categoryService.createCategory(categoryParam));
        String expectedMessage = "campo id erroneo";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testNoneFieldIsNull() {
        when(categoryRepository.save(Mockito.any(Category.class))).thenReturn(sampleCategory);
        CategoryDto categoryParam = new CategoryFactory().setIdCategory(null).setNameCategory(null).newInstanceDto();
        Exception exception = assertThrows(CategoryException.class, () -> categoryService.createCategory(categoryParam));
        String expectedMessage = "los campos están vacios";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testObjetIsNotNull() {
        when(categoryRepository.save(Mockito.any(Category.class))).thenReturn(sampleCategory);
        CategoryDto categoryParam = new CategoryFactory().setIdCategory(null).setDescriptionCategory(null).newInstanceDto();
        Exception exception = assertThrows(CategoryException.class, () -> categoryService.createCategory(categoryParam));
        String actualMessage = exception.getMessage();
        assertThat(actualMessage).isNotBlank();
    }

    @Test
    void testTextoingresadoAgregado() {
        when(categoryRepository.save(Mockito.any(Category.class))).thenReturn(sampleCategory);
        CategoryDto categoryParam = new CategoryFactory().setIdCategory(null).newInstanceDto();
        CategoryDto category = categoryService.createCategory(categoryParam);
        assertThat(category).isNotNull();
    }

    @Test
    void updateCampoEsNull() {
        when(categoryRepository.save(Mockito.any(Category.class))).thenReturn(sampleCategory);
        CategoryDto categoryParam = new CategoryFactory().setIdCategory(null).newInstanceDto();
        Exception exception = assertThrows(CategoryException.class, () -> categoryService.updateCategory(categoryParam));
        String expectedMessage = "campo id erroneo";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void updateTestIdFieldIsNotNull() {
        when(categoryRepository.save(Mockito.any(Category.class))).thenReturn(sampleCategory);
        CategoryDto categoryParam = new CategoryFactory().newInstanceDto();
        CategoryDto category = categoryService.updateCategory(categoryParam);
        assertThat(category.getIdCategory()).isNotNull();
    }

    @Test
    void updateTestNoneFieldIsEmpty() {
        when(categoryRepository.save(Mockito.any(Category.class))).thenReturn(sampleCategory);
        CategoryDto categoryParam = new CategoryFactory().setDescriptionCategory("").newInstanceDto();
        Exception exception = assertThrows(CategoryException.class, () -> categoryService.updateCategory(categoryParam));
        String expectedMessage = "los campos están vacios";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void updateTestNoneFieldIsNull() {
        when(categoryRepository.save(Mockito.any(Category.class))).thenReturn(sampleCategory);
        CategoryDto categoryParam = new CategoryFactory().setStatusCategory(null).newInstanceDto();
        Exception exception = assertThrows(CategoryException.class, () -> categoryService.updateCategory(categoryParam));
        String expectedMessage = "los campos están vacios";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void updateTestObjetIsNull() {
        when(categoryRepository.save(Mockito.any(Category.class))).thenThrow(IllegalArgumentException.class);
        Exception exception = assertThrows(CategoryException.class, () -> categoryService.updateCategory(null));
        String expectedMessage = "Error al construir.";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }


}
