package com.practica.empresarial.veterinaria.service;

import com.practica.empresarial.veterinaria.exceptions.ServiceVeterinaryException;
import com.practica.empresarial.veterinaria.factorie.ServiceVeterinaryFactory;
import com.practica.empresarial.veterinaria.model.ServiceVeterinary;
import com.practica.empresarial.veterinaria.model.dto.ServiceVeterinaryDto;
import com.practica.empresarial.veterinaria.model.transform.ServiceVeterinaryTransform;
import com.practica.empresarial.veterinaria.repository.ServiceVeterinaryRepository;
import com.practica.empresarial.veterinaria.utils.MessageUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SpringBootTest
class ServiceVeterinaryServicesTest {

    @Autowired
    private MessageUtil messageUtil;
    private ServiceVeterinaryServices serviceVeterinaryServices;
    private ServiceVeterinaryRepository serviceVeterinaryRepository;

    private ServiceVeterinary sampleServiceVeterinary;
    private ServiceVeterinaryDto sampleServiceVeterinaryDtoCreate;
    private ServiceVeterinaryDto sampleServiceVeterinaryDto;
    private List<ServiceVeterinary> sampleDataTest;
    private List<ServiceVeterinary> sampleDataTestEmpty;

    @BeforeEach
    public void setup() {
        serviceVeterinaryRepository = mock(ServiceVeterinaryRepository.class);
        ServiceVeterinaryTransform serviceVeterinaryTransform = new ServiceVeterinaryTransform();

        serviceVeterinaryServices = new ServiceVeterinaryServices(serviceVeterinaryRepository, serviceVeterinaryTransform);
        sampleServiceVeterinary = new ServiceVeterinaryFactory().newInstance();
        sampleServiceVeterinaryDto = new ServiceVeterinaryFactory().newInstanceDto();
        sampleServiceVeterinaryDtoCreate = new ServiceVeterinaryFactory().setIdService(null).newInstanceDto();

        sampleDataTest = new ArrayList<>();
        sampleDataTestEmpty = new ArrayList<>();
        sampleDataTest.add(sampleServiceVeterinary);

    }

    @Test
    void insertServiceValidFields() {
        when(serviceVeterinaryRepository.save(Mockito.any(ServiceVeterinary.class))).thenReturn(sampleServiceVeterinary);
        ServiceVeterinaryDto actualAnswer = serviceVeterinaryServices.createServices(sampleServiceVeterinaryDtoCreate);
        assertThat(actualAnswer.getIdService()).isEqualTo(sampleServiceVeterinaryDto.getIdService());
    }

    @Test
    void insertServiceIdFieldNull() {
        ServiceVeterinaryDto serviceVeterinary = new ServiceVeterinaryFactory().setIdService(5L).newInstanceDto();
        Exception exception = assertThrows(ServiceVeterinaryException.class, () -> serviceVeterinaryServices.createServices(serviceVeterinary));
        String actualMessage = exception.getMessage();
        String expectedMessage = "campo id erroneo";
        assertThat(actualMessage).contains(expectedMessage);
    }

    @Test
    void insertServiceNullFields() {
        ServiceVeterinaryDto serviceVeterinary = new ServiceVeterinaryFactory().setIdService(null).setNameService(null).setValueService(null).setQuantityService(null).setDescriptionService(null).setIdService(null).newInstanceDto();
        Exception exception = assertThrows(ServiceVeterinaryException.class, () -> serviceVeterinaryServices.createServices(serviceVeterinary));
        String actualMessage = exception.getMessage();
        String expectedMessage = "los campos están vacios";
        assertThat(actualMessage).contains(expectedMessage);

    }

    @Test
    void insertServiceDescriptionsNullFields() {
        ServiceVeterinaryDto serviceVeterinary = new ServiceVeterinaryFactory().setIdService(null).setNameService("null").setValueService(null).setQuantityService(null).setDescriptionService(null).setStatusService(null).newInstanceDto();
        Exception exception = assertThrows(ServiceVeterinaryException.class, () -> serviceVeterinaryServices.createServices(serviceVeterinary));
        String actualMessage = exception.getMessage();
        String expectedMessage = "los campos están vacios";
        assertThat(actualMessage).contains(expectedMessage);

    }

    @Test
    void insertServiceValueNullFields() {
        ServiceVeterinaryDto serviceVeterinary = new ServiceVeterinaryFactory().setIdService(null).setNameService("null").setValueService(null).setQuantityService(null).setDescriptionService("null").setStatusService(null).newInstanceDto();
        Exception exception = assertThrows(ServiceVeterinaryException.class, () -> serviceVeterinaryServices.createServices(serviceVeterinary));
        String actualMessage = exception.getMessage();
        String expectedMessage = "los campos están vacios";
        assertThat(actualMessage).contains(expectedMessage);

    }

    @Test
    void insertServiceQuantyNullFields() {
        ServiceVeterinaryDto serviceVeterinary = new ServiceVeterinaryFactory().setIdService(null).setNameService("null").setValueService(1F).setQuantityService(null).setDescriptionService("null").setStatusService(null).newInstanceDto();
        Exception exception = assertThrows(ServiceVeterinaryException.class, () -> serviceVeterinaryServices.createServices(serviceVeterinary));
        String actualMessage = exception.getMessage();
        String expectedMessage = "los campos están vacios";
        assertThat(actualMessage).contains(expectedMessage);

    }

    @Test
    void insertServiceIdCategoryNullFields() {
        ServiceVeterinaryDto serviceVeterinary = new ServiceVeterinaryFactory().setIdService(null).setNameService("null").setValueService(1F).setQuantityService(1L).setDescriptionService("null").setStatusService(null).newInstanceDto();
        Exception exception = assertThrows(ServiceVeterinaryException.class, () -> serviceVeterinaryServices.createServices(serviceVeterinary));
        String actualMessage = exception.getMessage();
        String expectedMessage = "los campos están vacios";
        assertThat(actualMessage).contains(expectedMessage);

    }

    @Test
    void insertServiceStatusNullFields() {
        ServiceVeterinaryDto serviceVeterinary = new ServiceVeterinaryFactory().setIdService(null).setNameService("null").setValueService(1F).setQuantityService(1L).setDescriptionService("null").setStatusService(null).newInstanceDto();
        Exception exception = assertThrows(ServiceVeterinaryException.class, () -> serviceVeterinaryServices.createServices(serviceVeterinary));
        String actualMessage = exception.getMessage();
        String expectedMessage = "los campos están vacios";
        assertThat(actualMessage).contains(expectedMessage);

    }

    @Test
    void insertNullService() {
        when(serviceVeterinaryRepository.save(sampleServiceVeterinary)).thenThrow(IllegalArgumentException.class);
        Exception exception = assertThrows(ServiceVeterinaryException.class, () -> serviceVeterinaryServices.createServices(null));
        String actualMessage = exception.getMessage();
        String expectedMessage = "Error al construir.";
        assertThat(actualMessage).contains(expectedMessage);

    }

    @Test
    void insertServiceValueFieldZero() {
        ServiceVeterinaryDto serviceVeterinary = new ServiceVeterinaryFactory().setIdService(null).setNameService("medicamentos").setValueService(0F).setQuantityService(1L).setDescriptionService("fecha de vacunacion").setStatusService(true).newInstanceDto();
        Exception exception = assertThrows(ServiceVeterinaryException.class, () -> serviceVeterinaryServices.createServices(serviceVeterinary));
        String actualMessage = exception.getMessage();
        String expectedMessage = "el valor ingresado no esta permitido";
        assertThat(actualMessage).isEqualTo(expectedMessage);

    }

    @Test
    void updateServiceValidFields() {
        when(serviceVeterinaryRepository.save(Mockito.any(ServiceVeterinary.class))).thenReturn(sampleServiceVeterinary);
        ServiceVeterinaryDto serviceVeterinary = new ServiceVeterinaryFactory().setIdService(1L).setNameService("medicamentos").setValueService(1000F).setQuantityService(1L).setDescriptionService("fecha de vacunacion").setStatusService(true).newInstanceDto();
        ServiceVeterinaryDto actualAnswer = serviceVeterinaryServices.updateService(serviceVeterinary);
        assertThat(actualAnswer).isNotNull();
    }

    @Test
    void updateServiceIdFieldNull() {
        when(serviceVeterinaryRepository.save(Mockito.any(ServiceVeterinary.class))).thenReturn(sampleServiceVeterinary);
        ServiceVeterinaryDto serviceVeterinary = new ServiceVeterinaryFactory().setIdService(null).setNameService("medicamentos").setValueService(1000F).setQuantityService(1L).setDescriptionService("fecha de vacunacion").setStatusService(true).newInstanceDto();
        Exception exception = assertThrows(ServiceVeterinaryException.class, () -> serviceVeterinaryServices.updateService(serviceVeterinary));
        String actualMessage = exception.getMessage();
        String expectedMessage = "campo id erroneo";
        assertThat(actualMessage).contains(expectedMessage);
    }

    @Test
    void updateServiceNullFields() {
        when(serviceVeterinaryRepository.save(Mockito.any(ServiceVeterinary.class))).thenReturn(sampleServiceVeterinary);
        ServiceVeterinaryDto serviceVeterinary = new ServiceVeterinaryFactory().setIdService(2L).setNameService(null).setValueService(null).setQuantityService(1L).setDescriptionService(null).setStatusService(null).newInstanceDto();
        Exception exception = assertThrows(ServiceVeterinaryException.class, () -> serviceVeterinaryServices.updateService(serviceVeterinary));
        String actualMessage = exception.getMessage();
        String expectedMessage = "los campos están vacios";
        assertThat(actualMessage).contains(expectedMessage);
    }

    @Test
    void updateNullService() {
        when(serviceVeterinaryRepository.save(Mockito.any(ServiceVeterinary.class))).thenReturn(sampleServiceVeterinary);
        Exception exception = assertThrows(ServiceVeterinaryException.class, () -> serviceVeterinaryServices.updateService(null));
        String actualMessage = exception.getMessage();
        String expectedMessage = "Error al construir.";
        assertThat(actualMessage).contains(expectedMessage);

    }

    @Test
    void updateServiceValueFieldZero() {
        when(serviceVeterinaryRepository.save(Mockito.any(ServiceVeterinary.class))).thenReturn(sampleServiceVeterinary);
        ServiceVeterinaryDto serviceVeterinary = new ServiceVeterinaryFactory().setIdService(2L).setNameService("medicamento").setValueService(0F).setQuantityService(1L).setDescriptionService("desparacitante").setStatusService(true).newInstanceDto();
        Exception exception = assertThrows(ServiceVeterinaryException.class, () -> serviceVeterinaryServices.updateService(serviceVeterinary));
        String actualMessage = exception.getMessage();
        String expectedMessage = "el valor ingresado no esta permitido";
        assertThat(actualMessage).isEqualTo(expectedMessage);
    }

    @Test
    void getExistingCategory() {
        when(serviceVeterinaryRepository.findAllByIdCategory(Mockito.anyInt())).thenReturn(sampleDataTest);
        Integer idCategory = 3;
        List<ServiceVeterinaryDto> dataConsulted = serviceVeterinaryServices.getAllServiceByType(idCategory);
        assertThat(dataConsulted).isNotEmpty();
    }

    @Test
    void getCategoryNotExisting() {
        Integer idCategory = 999;
        when(serviceVeterinaryRepository.findAllByIdCategory(Mockito.anyInt())).thenReturn(sampleDataTestEmpty);
        List<ServiceVeterinaryDto> dataConsulted = serviceVeterinaryServices.getAllServiceByType(idCategory);
        assertThat(dataConsulted).isEmpty();
    }

    @Test
    void getTypeNull() {
        when(serviceVeterinaryRepository.findAll()).thenReturn(sampleDataTestEmpty);
        List<ServiceVeterinaryDto> dataConsulted = serviceVeterinaryServices.getAllServiceByType(null);
        assertThat(dataConsulted).isEmpty();
    }

    @Test
    void getNameExisting() {
        when(serviceVeterinaryRepository.findAllByNameService(sampleServiceVeterinary.getNameService())).thenReturn(sampleDataTest);
        String name = "peluqueria";
        List<ServiceVeterinaryDto> dataConsulted = serviceVeterinaryServices.getAllServiceByNameService(name);
        assertThat(dataConsulted).isNotEmpty();
    }

    @Test
    void getNameNotExisting() {
        String name = "musica";
        when(serviceVeterinaryRepository.findAllByNameService(sampleServiceVeterinary.getNameService())).thenReturn(sampleDataTestEmpty);
        List<ServiceVeterinaryDto> dataConsulted = serviceVeterinaryServices.getAllServiceByNameService(name);
        assertThat(dataConsulted).isEmpty();
    }

    @Test
    void getExistingNameToValidateObject() {
        when(serviceVeterinaryRepository.findAll()).thenReturn(sampleDataTest);
        String name = "Ingrith";
        List<ServiceVeterinaryDto> dataConsulted = serviceVeterinaryServices.getAllServiceByNameService(name);
        for (ServiceVeterinaryDto serviceVeterinary : dataConsulted) {
            assertThat(serviceVeterinary.getNameService()).isEqualTo(name);
        }
    }

    @Test
    void getNullType() {
        when(serviceVeterinaryRepository.findAll()).thenReturn(sampleDataTestEmpty);
        List<ServiceVeterinaryDto> dataConsulted = serviceVeterinaryServices.getAllServiceByType(null);
        assertThat(dataConsulted).isEmpty();
    }


}