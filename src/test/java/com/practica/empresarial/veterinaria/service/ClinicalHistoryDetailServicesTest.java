package com.practica.empresarial.veterinaria.service;


import com.practica.empresarial.veterinaria.exceptions.ClinicalHistoryDetailException;
import com.practica.empresarial.veterinaria.factorie.ClinicalHistoryDetailFactorie;
import com.practica.empresarial.veterinaria.model.ClinicalHistoryDetail;
import com.practica.empresarial.veterinaria.model.dto.ClinicalHistoryDetailDto;
import com.practica.empresarial.veterinaria.model.transform.ClinicalHistoryDetailTransform;
import com.practica.empresarial.veterinaria.repository.ClinicalHistoryDetailRepository;
import com.practica.empresarial.veterinaria.utils.MessageUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SpringBootTest
class ClinicalHistoryDetailServicesTest {

    @Autowired
    MessageUtil messageUtil;
    private ClinicalHistoryDetailServices clinicalHistoryDetailServices;
    private ClinicalHistoryDetailRepository clinicalHistoryDetailRepository;

    private ClinicalHistoryDetail sampleClinical;
    private ClinicalHistoryDetailDto sampleClinicalDto;
    private ClinicalHistoryDetailDto sampleClinicalDtoCreate;

    @BeforeEach
    public void setup() {
        clinicalHistoryDetailRepository = mock(ClinicalHistoryDetailRepository.class);
        ClinicalHistoryDetailTransform clinicalHistoryDetailTransform = new ClinicalHistoryDetailTransform();

        clinicalHistoryDetailServices = new ClinicalHistoryDetailServices(clinicalHistoryDetailTransform, clinicalHistoryDetailRepository, messageUtil);
        sampleClinical = new ClinicalHistoryDetailFactorie().newInstance();
        sampleClinicalDto = new ClinicalHistoryDetailFactorie().newInstanceDto();
        sampleClinicalDtoCreate = new ClinicalHistoryDetailFactorie().setIdDetailHc(null).newInstanceDto();

    }

    @Test
    void testValidFields() {
        when(clinicalHistoryDetailRepository.save(Mockito.any(ClinicalHistoryDetail.class))).thenReturn(sampleClinical);
        ClinicalHistoryDetailDto actualAnswer = clinicalHistoryDetailServices.createClinicalHistory(sampleClinicalDtoCreate);
        assertThat(actualAnswer.getIdDetailHC()).isEqualTo(sampleClinical.getIdDetailHC());
    }

    @Test
    void testValidFieldsNull() {
        when(clinicalHistoryDetailRepository.save(Mockito.any(ClinicalHistoryDetail.class))).thenReturn(null);
        Exception exception = assertThrows(ClinicalHistoryDetailException.class, () -> clinicalHistoryDetailServices.createClinicalHistory(null));
        String actualMessage = exception.getMessage();
        String expectedMessage = "Error al construir.";
        assertThat(actualMessage).contains(expectedMessage);

    }

    @Test
    void testIdFieldNotNull() {
        when(clinicalHistoryDetailRepository.save(Mockito.any(ClinicalHistoryDetail.class))).thenReturn(sampleClinical);
        ClinicalHistoryDetailDto clinical = new ClinicalHistoryDetailFactorie().newInstanceDto();
        Exception exception = assertThrows(ClinicalHistoryDetailException.class, () -> clinicalHistoryDetailServices.createClinicalHistory(clinical));
        String actualAnswer = exception.getMessage();
        String expectedMessage = "campo id erroneo";
        assertThat(actualAnswer).contains(expectedMessage);
    }

    @Test
    void testNullFields() {
        ClinicalHistoryDetailDto clinical = new ClinicalHistoryDetailFactorie().setIdDetailHc(null).setTemperature(null).newInstanceDto();
        Exception exception = assertThrows(ClinicalHistoryDetailException.class, () -> clinicalHistoryDetailServices.createClinicalHistory(clinical));
        String actualAnswer = exception.getMessage();
        String expectedMessage = "los campos están vacios";
        assertThat(actualAnswer).contains(expectedMessage);
    }

    @Test
    void testTemperatureNullFields() {
        when(clinicalHistoryDetailRepository.save(Mockito.any(ClinicalHistoryDetail.class))).thenReturn(sampleClinical);
        ClinicalHistoryDetailDto clinical = new ClinicalHistoryDetailFactorie().setIdDetailHc(null).setTemperature(null).newInstanceDto();
        Exception exception = assertThrows(ClinicalHistoryDetailException.class, () -> clinicalHistoryDetailServices.createClinicalHistory(clinical));
        String actualAnswer = exception.getMessage();
        String expectedMessage = "los campos están vacios";
        assertThat(actualAnswer).contains(expectedMessage);
    }

    @Test
    void testWeigthNullFields() {
        when(clinicalHistoryDetailRepository.save(Mockito.any(ClinicalHistoryDetail.class))).thenReturn(sampleClinical);
        ClinicalHistoryDetailDto clinical = new ClinicalHistoryDetailFactorie().setIdDetailHc(null).setWeight(null).newInstanceDto();
        Exception exception = assertThrows(ClinicalHistoryDetailException.class, () -> clinicalHistoryDetailServices.createClinicalHistory(clinical));
        String actualAnswer = exception.getMessage();
        String expectedMessage = "los campos están vacios";
        assertThat(actualAnswer).contains(expectedMessage);
    }

    @Test
    void testHeartRateNullFields() {
        when(clinicalHistoryDetailRepository.save(Mockito.any(ClinicalHistoryDetail.class))).thenReturn(sampleClinical);
        ClinicalHistoryDetailDto clinical = new ClinicalHistoryDetailFactorie().setIdDetailHc(null).setHeartRate(null).newInstanceDto();
        Exception exception = assertThrows(ClinicalHistoryDetailException.class, () -> clinicalHistoryDetailServices.createClinicalHistory(clinical));
        String actualAnswer = exception.getMessage();
        String expectedMessage = "los campos están vacios";
        assertThat(actualAnswer).contains(expectedMessage);
    }

    @Test
    void testbreathingFrequencyNullFields() {
        when(clinicalHistoryDetailRepository.save(Mockito.any(ClinicalHistoryDetail.class))).thenReturn(sampleClinical);
        ClinicalHistoryDetailDto clinical = new ClinicalHistoryDetailFactorie().setIdDetailHc(null).setBreathingFrequency(null).newInstanceDto();
        Exception exception = assertThrows(ClinicalHistoryDetailException.class, () -> clinicalHistoryDetailServices.createClinicalHistory(clinical));
        String actualAnswer = exception.getMessage();
        String expectedMessage = "los campos están vacios";
        assertThat(actualAnswer).contains(expectedMessage);
    }

    @Test
    void testAdmissionDateNullFields() {
        when(clinicalHistoryDetailRepository.save(Mockito.any(ClinicalHistoryDetail.class))).thenReturn(sampleClinical);
        ClinicalHistoryDetailDto clinical = new ClinicalHistoryDetailFactorie().setIdDetailHc(null).setAdmissionDate(null).newInstanceDto();
        Exception exception = assertThrows(ClinicalHistoryDetailException.class, () -> clinicalHistoryDetailServices.createClinicalHistory(clinical));
        String actualAnswer = exception.getMessage();
        String expectedMessage = "los campos están vacios";
        assertThat(actualAnswer).contains(expectedMessage);
    }

    @Test
    void testIdClinicalHistoryNullFields() {
        when(clinicalHistoryDetailRepository.save(Mockito.any(ClinicalHistoryDetail.class))).thenReturn(sampleClinical);
        ClinicalHistoryDetailDto clinical = new ClinicalHistoryDetailFactorie().setIdDetailHc(null).setIdClinicalHistory(null).newInstanceDto();
        Exception exception = assertThrows(ClinicalHistoryDetailException.class, () -> clinicalHistoryDetailServices.createClinicalHistory(clinical));
        String actualAnswer = exception.getMessage();
        String expectedMessage = "los campos están vacios";
        assertThat(actualAnswer).contains(expectedMessage);
    }

    @Test
    void testFeedingNullFields() {
        when(clinicalHistoryDetailRepository.save(Mockito.any(ClinicalHistoryDetail.class))).thenReturn(sampleClinical);
        ClinicalHistoryDetailDto clinical = new ClinicalHistoryDetailFactorie().setIdDetailHc(null).setFeeding(null).newInstanceDto();
        Exception exception = assertThrows(ClinicalHistoryDetailException.class, () -> clinicalHistoryDetailServices.createClinicalHistory(clinical));
        String actualAnswer = exception.getMessage();
        String expectedMessage = "los campos están vacios";
        assertThat(actualAnswer).contains(expectedMessage);
    }

    @Test
    void testDewormingDateNullFields() {
        when(clinicalHistoryDetailRepository.save(Mockito.any(ClinicalHistoryDetail.class))).thenReturn(sampleClinical);
        ClinicalHistoryDetailDto clinical = new ClinicalHistoryDetailFactorie().setIdDetailHc(null).setDewormingDate(null).newInstanceDto();
        Exception exception = assertThrows(ClinicalHistoryDetailException.class, () -> clinicalHistoryDetailServices.createClinicalHistory(clinical));
        String actualAnswer = exception.getMessage();
        String expectedMessage = "los campos están vacios";
        assertThat(actualAnswer).contains(expectedMessage);
    }

    @Test
    void testHabitatNullFields() {
        when(clinicalHistoryDetailRepository.save(Mockito.any(ClinicalHistoryDetail.class))).thenReturn(sampleClinical);
        ClinicalHistoryDetailDto clinical = new ClinicalHistoryDetailFactorie().setIdDetailHc(null).setHabitat(null).newInstanceDto();
        Exception exception = assertThrows(ClinicalHistoryDetailException.class, () -> clinicalHistoryDetailServices.createClinicalHistory(clinical));
        String actualAnswer = exception.getMessage();
        String expectedMessage = "los campos están vacios";
        assertThat(actualAnswer).contains(expectedMessage);
    }

    @Test
    void testCapillaryTimeNullFields() {
        when(clinicalHistoryDetailRepository.save(Mockito.any(ClinicalHistoryDetail.class))).thenReturn(sampleClinical);
        ClinicalHistoryDetailDto clinical = new ClinicalHistoryDetailFactorie().setIdDetailHc(null).setCapillaryTime(null).newInstanceDto();
        Exception exception = assertThrows(ClinicalHistoryDetailException.class, () -> clinicalHistoryDetailServices.createClinicalHistory(clinical));
        String actualAnswer = exception.getMessage();
        String expectedMessage = "los campos están vacios";
        assertThat(actualAnswer).contains(expectedMessage);
    }

    @Test
    void testReproductiveStatusNullFields() {
        when(clinicalHistoryDetailRepository.save(Mockito.any(ClinicalHistoryDetail.class))).thenReturn(sampleClinical);
        ClinicalHistoryDetailDto clinical = new ClinicalHistoryDetailFactorie().setIdDetailHc(null).setReproductiveStatus(null).newInstanceDto();
        Exception exception = assertThrows(ClinicalHistoryDetailException.class, () -> clinicalHistoryDetailServices.createClinicalHistory(clinical));
        String actualAnswer = exception.getMessage();
        String expectedMessage = "los campos están vacios";
        assertThat(actualAnswer).contains(expectedMessage);
    }

    @Test
    void testObservationsNullFields() {
        when(clinicalHistoryDetailRepository.save(Mockito.any(ClinicalHistoryDetail.class))).thenReturn(sampleClinical);
        ClinicalHistoryDetailDto clinical = new ClinicalHistoryDetailFactorie().setIdDetailHc(null).setObservations(null).newInstanceDto();
        Exception exception = assertThrows(ClinicalHistoryDetailException.class, () -> clinicalHistoryDetailServices.createClinicalHistory(clinical));
        String actualAnswer = exception.getMessage();
        String expectedMessage = "los campos están vacios";
        assertThat(actualAnswer).contains(expectedMessage);
    }

    @Test
    void testIdCollaboratorNullFields() {
        when(clinicalHistoryDetailRepository.save(Mockito.any(ClinicalHistoryDetail.class))).thenReturn(sampleClinical);
        ClinicalHistoryDetailDto clinical = new ClinicalHistoryDetailFactorie().setIdDetailHc(null).setIdCollaborator(null).newInstanceDto();
        Exception exception = assertThrows(ClinicalHistoryDetailException.class, () -> clinicalHistoryDetailServices.createClinicalHistory(clinical));
        String actualAnswer = exception.getMessage();
        String expectedMessage = "los campos están vacios";
        assertThat(actualAnswer).contains(expectedMessage);
    }

    @Test
    void testTemperatureValueFields() {
        when(clinicalHistoryDetailRepository.save(Mockito.any(ClinicalHistoryDetail.class))).thenReturn(sampleClinical);
        ClinicalHistoryDetailDto clinical = new ClinicalHistoryDetailFactorie().setIdDetailHc(null).setTemperature(0f).newInstanceDto();
        Exception exception = assertThrows(ClinicalHistoryDetailException.class, () -> clinicalHistoryDetailServices.createClinicalHistory(clinical));
        String actualAnswer = exception.getMessage();
        String expectedMessage = "el valor ingresado no esta permitido";
        assertThat(actualAnswer).contains(expectedMessage);
    }

    @Test
    void testWeightValueFields() {
        when(clinicalHistoryDetailRepository.save(Mockito.any(ClinicalHistoryDetail.class))).thenReturn(sampleClinical);
        ClinicalHistoryDetailDto clinical = new ClinicalHistoryDetailFactorie().setIdDetailHc(null).setWeight(0f).newInstanceDto();
        Exception exception = assertThrows(ClinicalHistoryDetailException.class, () -> clinicalHistoryDetailServices.createClinicalHistory(clinical));
        String actualAnswer = exception.getMessage();
        String expectedMessage = "el valor ingresado no esta permitido";
        assertThat(actualAnswer).contains(expectedMessage);
    }

    @Test
    void testHeartRateValueFields() {
        when(clinicalHistoryDetailRepository.save(Mockito.any(ClinicalHistoryDetail.class))).thenReturn(sampleClinical);
        ClinicalHistoryDetailDto clinical = new ClinicalHistoryDetailFactorie().setIdDetailHc(null).setHeartRate(0f).newInstanceDto();
        Exception exception = assertThrows(ClinicalHistoryDetailException.class, () -> clinicalHistoryDetailServices.createClinicalHistory(clinical));
        String actualAnswer = exception.getMessage();
        String expectedMessage = "el valor ingresado no esta permitido";
        assertThat(actualAnswer).contains(expectedMessage);
    }

    @Test
    void testBreathingFrequencyValueFields() {
        when(clinicalHistoryDetailRepository.save(Mockito.any(ClinicalHistoryDetail.class))).thenReturn(sampleClinical);
        ClinicalHistoryDetailDto clinical = new ClinicalHistoryDetailFactorie().setIdDetailHc(null).setBreathingFrequency(0f).newInstanceDto();
        Exception exception = assertThrows(ClinicalHistoryDetailException.class, () -> clinicalHistoryDetailServices.createClinicalHistory(clinical));
        String actualAnswer = exception.getMessage();
        String expectedMessage = "el valor ingresado no esta permitido";
        assertThat(actualAnswer).contains(expectedMessage);
    }

    @Test
    void testIdClinicalHistoryValueFields() {
        when(clinicalHistoryDetailRepository.save(Mockito.any(ClinicalHistoryDetail.class))).thenReturn(sampleClinical);
        ClinicalHistoryDetailDto clinical = new ClinicalHistoryDetailFactorie().setIdDetailHc(null).setIdClinicalHistory(0L).newInstanceDto();
        Exception exception = assertThrows(ClinicalHistoryDetailException.class, () -> clinicalHistoryDetailServices.createClinicalHistory(clinical));
        String actualAnswer = exception.getMessage();
        String expectedMessage = "el valor ingresado no esta permitido";
        assertThat(actualAnswer).contains(expectedMessage);
    }

    @Test
    void testCapillaryTimeValueFields() {
        when(clinicalHistoryDetailRepository.save(Mockito.any(ClinicalHistoryDetail.class))).thenReturn(sampleClinical);
        ClinicalHistoryDetailDto clinical = new ClinicalHistoryDetailFactorie().setIdDetailHc(null).setCapillaryTime(0f).newInstanceDto();
        Exception exception = assertThrows(ClinicalHistoryDetailException.class, () -> clinicalHistoryDetailServices.createClinicalHistory(clinical));
        String actualAnswer = exception.getMessage();
        String expectedMessage = "el valor ingresado no esta permitido";
        assertThat(actualAnswer).contains(expectedMessage);
    }

    @Test
    void testIdCollaboratorValueFields() {
        when(clinicalHistoryDetailRepository.save(Mockito.any(ClinicalHistoryDetail.class))).thenReturn(sampleClinical);
        ClinicalHistoryDetailDto clinical = new ClinicalHistoryDetailFactorie().setIdDetailHc(null).setIdCollaborator(0L).newInstanceDto();
        Exception exception = assertThrows(ClinicalHistoryDetailException.class, () -> clinicalHistoryDetailServices.createClinicalHistory(clinical));
        String actualAnswer = exception.getMessage();
        String expectedMessage = "el valor ingresado no esta permitido";
        assertThat(actualAnswer).contains(expectedMessage);
    }

}