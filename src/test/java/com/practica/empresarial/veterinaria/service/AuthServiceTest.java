package com.practica.empresarial.veterinaria.service;

import com.practica.empresarial.veterinaria.configuration.JwtToken;
import com.practica.empresarial.veterinaria.factorie.UserVeterinaryFactory;
import com.practica.empresarial.veterinaria.model.dto.UserVeterinaryDto;
import com.practica.empresarial.veterinaria.model.transform.UserVeterinaryTransform;
import com.practica.empresarial.veterinaria.repository.UserVeterinaryRepository;
import com.practica.empresarial.veterinaria.utils.MessageUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;

import static org.mockito.Mockito.mock;

@SpringBootTest
class AuthServiceTest {

    @Autowired
    MessageUtil messageUtil;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtToken jwtToken;


    private JwtUserDetailsService jwtUserDetailsService;
    private UserVeterinaryRepository userVeterinaryRepository;
    private AuthService authService;
    private UserVeterinaryDto sampleUserDto;

    @BeforeEach
    public void setup() {
        userVeterinaryRepository = mock(UserVeterinaryRepository.class);
        jwtUserDetailsService = mock(JwtUserDetailsService.class);
        UserVeterinaryTransform userVeterinaryTransform = new UserVeterinaryTransform();
        sampleUserDto = new UserVeterinaryFactory().newInstanceDto();
    }

    @Test
    void registerUser() {

    }

}