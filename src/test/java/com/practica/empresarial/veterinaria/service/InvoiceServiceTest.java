package com.practica.empresarial.veterinaria.service;

import com.practica.empresarial.veterinaria.model.Invoice;
import com.practica.empresarial.veterinaria.model.dto.InvoiceDto;
import com.practica.empresarial.veterinaria.model.transform.InvoiceTransform;
import com.practica.empresarial.veterinaria.repository.DetailInvoiceRepository;
import com.practica.empresarial.veterinaria.repository.InvoiceRepository;
import com.practica.empresarial.veterinaria.utils.MailUtil;
import com.practica.empresarial.veterinaria.utils.MessageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;


@SpringBootTest
class InvoiceServiceTest {

    @Autowired
    private MessageUtil messageUtil;
    private DetailVaccineServices detailVaccineServices;
    private InvoiceServices invoiceServices;
    private InvoiceServices detailInvoiceServices;
    private InvoiceRepository invoiceRepository;
    private MailUtil mailUtil;
    private DetailInvoiceRepository detailInvoiceService;
    private InvoiceTransform invoiceTransform;

    private Invoice sampleInvoice;
    private InvoiceDto sampleInvoiceDtoCreate;
    private InvoiceDto sampleInvoiceDto;
    private List<Invoice> sampleDataTest;
    private List<Invoice> sampleDataTestEmpty;

     /*@BeforeEach
    public void setup() {
        invoiceRepository = mock(InvoiceRepository.class);
        mailUtil = mock(MailUtil.class);
        InvoiceTransform invoiceTransform = new InvoiceTransform();

        invoiceServices = new InvoiceServices(invoiceTransform, invoiceRepository, messageUtil, mailUtil,detailInvoiceService,);

        sampleInvoice = Invoice.builder()
                .idInvoice(1L)
                .dateInvoice(new Date())
                .value(15000f)
                .value(15000f)
                .discount(15000f)
                .totalPrice(15000f)
                .idUser(1)
                .idBranch(1)
                .build();

        sampleInvoiceDto = InvoiceDto.builder()
                .idInvoice(1L)
                .dateInvoice(new Date())
                .value(15000f)
                .value(15000f)
                .discount(15000f)
                .totalPrice(15000f)
                .idUser(1)
                .idBranch(1)
                .build();

        sampleInvoiceDtoCreate = InvoiceDto.builder()
                .idInvoice(1L)
                .dateInvoice(new Date())
                .value(15000f)
                .value(15000f)
                .discount(15000f)
                .totalPrice(15000f)
                .idUser(1)
                .idBranch(1)
                .build();

        sampleDataTest = new ArrayList<>();
        sampleDataTestEmpty = new ArrayList<>();
        sampleDataTest.add(sampleInvoice);
    }

    @Test
    void getAllByIdUser() {
    }*/


}