package com.practica.empresarial.veterinaria.service;

import com.practica.empresarial.veterinaria.exceptions.ExamDetailException;
import com.practica.empresarial.veterinaria.factorie.ExamDetailFactory;
import com.practica.empresarial.veterinaria.model.ExamDetail;
import com.practica.empresarial.veterinaria.model.dto.ExamDetailDto;
import com.practica.empresarial.veterinaria.model.transform.ExamDetailTransform;
import com.practica.empresarial.veterinaria.repository.ExamDetailRepository;
import com.practica.empresarial.veterinaria.utils.MessageUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SpringBootTest
class ExamDetailServicesTest {

    @Autowired
    private MessageUtil messageUtil;
    private ExamDetailRepository examDetailRepository;
    private ExamDetailService examDetailService;
    private List<ExamDetail> sampleDataTestEmpty;
    private List<ExamDetail> sampleDataTest;
    private ExamDetail sample;

    @BeforeEach
    void setup() {
        examDetailRepository = mock(ExamDetailRepository.class);
        ExamDetailTransform examDetailTransform = new ExamDetailTransform();
        examDetailService = new ExamDetailService(examDetailRepository, examDetailTransform, messageUtil);
        sample = new ExamDetailFactory().newInstance();
        sampleDataTest = new ArrayList<>();
        sampleDataTestEmpty = new ArrayList<>();
        sampleDataTest.add(sample);
    }

    @Test
    void getNoExistingExam() {
        Long id = 33L;
        when(examDetailRepository.findAllByIdExam(Mockito.anyLong())).thenReturn(sampleDataTestEmpty);
        List<ExamDetailDto> dataConsulted = examDetailService.getAllExamDetailByIdExam(id);
        assertThat(dataConsulted).isEmpty();
    }

    @Test
    void getExistingExam() {
        Long id = 33L;
        when(examDetailRepository.findAllByIdExam(Mockito.anyLong())).thenReturn(sampleDataTest);
        List<ExamDetailDto> dataConsulted = examDetailService.getAllExamDetailByIdExam(id);
        assertThat(dataConsulted).isNotEmpty();
    }

    @Test
    void getExamIdNull() {
        when(examDetailRepository.findAllByIdExam(null)).thenThrow(IllegalArgumentException.class);
        Exception exception = assertThrows(ExamDetailException.class, () -> examDetailService.getAllExamDetailByIdExam(null));
        String expectedMessage = "el campo está vacio";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }
}
