package com.practica.empresarial.veterinaria.service;

import com.practica.empresarial.veterinaria.exceptions.ClinicalHistoryException;
import com.practica.empresarial.veterinaria.factorie.ClinicalHistoryFactorie;
import com.practica.empresarial.veterinaria.model.ClinicalHistory;
import com.practica.empresarial.veterinaria.model.dto.ClinicalHistoryDto;
import com.practica.empresarial.veterinaria.model.transform.ClinicalHistoryTransform;
import com.practica.empresarial.veterinaria.repository.ClinicalHistoryRepository;
import com.practica.empresarial.veterinaria.utils.MessageUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SpringBootTest
class ClinicalHistoryServicesTest {

    @Autowired
    MessageUtil messageUtil;
    private ClinicalHistoryServices clinicalHistoryServices;
    private ClinicalHistoryRepository clinicalHistoryRepository;

    private ClinicalHistory sampleClinical;
    private ClinicalHistoryDto sampleClinicalDto;
    private ClinicalHistoryDto sampleClinicalDtoCreate;

    @BeforeEach
    public void setup() {
        clinicalHistoryRepository = mock(ClinicalHistoryRepository.class);
        ClinicalHistoryTransform clinicalHistoryTransform = new ClinicalHistoryTransform();

        clinicalHistoryServices = new ClinicalHistoryServices(clinicalHistoryRepository, clinicalHistoryTransform, messageUtil);

        sampleClinical = new ClinicalHistoryFactorie().newInstance();
        sampleClinicalDto = new ClinicalHistoryFactorie().newInstanceDto();
        sampleClinicalDtoCreate = new ClinicalHistoryFactorie().setIdClinicalHistory(null).newInstanceDto();

    }

    @Test
    void insertClinicalHistoryDetailValidFields() {
        when(clinicalHistoryRepository.save(Mockito.any(ClinicalHistory.class))).thenReturn(sampleClinical);
        ClinicalHistoryDto actualAnswer = clinicalHistoryServices.createClinicalHistory(sampleClinicalDtoCreate);
        assertThat(actualAnswer.getIdClinicalHistory()).isEqualTo(sampleClinical.getIdClinicalHistory());
    }

    @Test
    void insertClinicalHistoryDetailValidFieldsNull() {
        when(clinicalHistoryRepository.save(Mockito.any(ClinicalHistory.class))).thenReturn(null);
        Exception exception = assertThrows(ClinicalHistoryException.class, () -> clinicalHistoryServices.createClinicalHistory(null));
        String actualMessage = exception.getMessage();
        String expectedMessage = "Error al construir.";
        assertThat(actualMessage).contains(expectedMessage);
    }


    @Test
    void insertClinicalHistoryDetailInvalidFields() {
        when(clinicalHistoryRepository.save(Mockito.any(ClinicalHistory.class))).thenReturn(sampleClinical);
        ClinicalHistoryDto errorCreate = new ClinicalHistoryFactorie().setIdClinicalHistory(null).setDateCreate(null).newInstanceDto();
        Exception exception = assertThrows(ClinicalHistoryException.class, () -> clinicalHistoryServices.createClinicalHistory(errorCreate));
        String actualMessage = exception.getMessage();
        String expectedMessage = "los campos están vacíos";
        assertThat(actualMessage).isEqualTo(expectedMessage);
    }

    @Test
    void insertClinicalHistoryDetailInvaslidFields() {
        when(clinicalHistoryRepository.save(Mockito.any(ClinicalHistory.class))).thenReturn(sampleClinical);
        ClinicalHistoryDto errorCreate = new ClinicalHistoryFactorie().newInstanceDto();
        Exception exception = assertThrows(ClinicalHistoryException.class, () -> clinicalHistoryServices.createClinicalHistory(errorCreate));
        String actualMessage = exception.getMessage();
        String expectedMessage = "campo id erroneo";
        assertThat(actualMessage).isEqualTo(expectedMessage);
    }

    @Test
    void insertClinicalHistoryDetailInvalidPetFields() {
        when(clinicalHistoryRepository.save(Mockito.any(ClinicalHistory.class))).thenReturn(sampleClinical);
        ClinicalHistoryDto errorCreate = new ClinicalHistoryFactorie().setIdClinicalHistory(null).setIdPet(null).newInstanceDto();
        Exception exception = assertThrows(ClinicalHistoryException.class, () -> clinicalHistoryServices.createClinicalHistory(errorCreate));
        String actualMessage = exception.getMessage();
        String expectedMessage = "los campos están vacíos";
        assertThat(actualMessage).isEqualTo(expectedMessage);
    }

    @Test
    void insertClinicalHistoryDetailInvalidPetFieldsForValue() {
        when(clinicalHistoryRepository.save(Mockito.any(ClinicalHistory.class))).thenReturn(sampleClinical);
        ClinicalHistoryDto errorCreate = new ClinicalHistoryFactorie().setIdClinicalHistory(null).setIdPet(0L).newInstanceDto();
        Exception exception = assertThrows(ClinicalHistoryException.class, () -> clinicalHistoryServices.createClinicalHistory(errorCreate));
        String actualMessage = exception.getMessage();
        String expectedMessage = "El valor ingresado no está permitido";
        assertThat(actualMessage).isEqualTo(expectedMessage);
    }

}