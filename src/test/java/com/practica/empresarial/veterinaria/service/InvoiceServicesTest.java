package com.practica.empresarial.veterinaria.service;

import com.practica.empresarial.veterinaria.factorie.InvoiceFactory;
import com.practica.empresarial.veterinaria.model.Invoice;
import com.practica.empresarial.veterinaria.model.dto.InvoiceDto;
import com.practica.empresarial.veterinaria.model.transform.InvoiceTransform;
import com.practica.empresarial.veterinaria.repository.InvoiceRepository;
import com.practica.empresarial.veterinaria.utils.MailUtil;
import com.practica.empresarial.veterinaria.utils.MessageUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SpringBootTest
class InvoiceServicesTest {
    public static final Integer ID_USER = 1;
    public static final Integer ID_USER_P = 0;

    @Autowired
    private MessageUtil messageUtil;
    private InvoiceServices invoiceServices;
    private InvoiceRepository invoiceRepository;

    private List<Invoice> sampleDataTest;
    private List<Invoice> sampleDataTestEmpty;

    InvoiceServicesTest() {
    }

    @BeforeEach
    void setUp() {
        DetailInvoiceService detailInvoiceService = mock(DetailInvoiceService.class);
        invoiceRepository = mock(InvoiceRepository.class);
        MailUtil mailUtil = mock(MailUtil.class);
        invoiceServices = new InvoiceServices(messageUtil, invoiceRepository, mailUtil, new InvoiceTransform(), detailInvoiceService);

        Invoice sampleInvoice = new InvoiceFactory().newInstance();
        sampleDataTest = new ArrayList<>();
        sampleDataTestEmpty = new ArrayList<>();
        sampleDataTest.add(sampleInvoice);

    }

    @Test
    void getExistingIdUser() {
        when(invoiceRepository.findAllByIdUser(Mockito.anyInt())).thenReturn(sampleDataTest);
        List<InvoiceDto> dataConsulted = invoiceServices.getAllByIdUser(ID_USER);
        assertThat(dataConsulted).isNotEmpty();
    }

    @Test
    void getNoExistingIdUser() {
        when(invoiceRepository.findAllByIdUser(Mockito.anyInt())).thenReturn(sampleDataTestEmpty);
        List<InvoiceDto> dataConsulted = invoiceServices.getAllByIdUser(ID_USER_P);
        assertThat(dataConsulted).isEmpty();
    }

    @Test
    void getExistingIdPet() {
        when(invoiceRepository.findAllByIdPet(Mockito.anyInt())).thenReturn(sampleDataTest);
        List<InvoiceDto> dataConsulted = invoiceServices.getAllByIdPet(ID_USER);
        assertThat(dataConsulted).isNotEmpty();
    }

    @Test
    void getNoExistingIdPet() {
        when(invoiceRepository.findAllByIdPet(Mockito.anyInt())).thenReturn(sampleDataTestEmpty);
        List<InvoiceDto> dataConsulted = invoiceServices.getAllByIdPet(ID_USER_P);
        assertThat(dataConsulted).isEmpty();
    }


}