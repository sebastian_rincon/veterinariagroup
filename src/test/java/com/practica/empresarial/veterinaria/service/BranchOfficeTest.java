package com.practica.empresarial.veterinaria.service;

import com.practica.empresarial.veterinaria.exceptions.BranchOfficeException;
import com.practica.empresarial.veterinaria.model.BranchOffice;
import com.practica.empresarial.veterinaria.model.dto.BranchOfficeDto;
import com.practica.empresarial.veterinaria.model.transform.BranchTransform;
import com.practica.empresarial.veterinaria.repository.BranchOfficeRepository;
import com.practica.empresarial.veterinaria.utils.MessageUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@SpringBootTest
class BranchOfficeTest {

    @Autowired
    private MessageUtil messageUtil;

    private BranchOfficeServices branchOfficeServices;
    private BranchOfficeRepository branchOfficeRepository;
    private BranchOffice sampleBranchOffice;
    private List<BranchOffice> dataTest;

    @BeforeEach
    public void setup() {
        branchOfficeRepository = mock(BranchOfficeRepository.class);
        BranchTransform branchTransform = new BranchTransform();
        branchOfficeServices = new BranchOfficeServices(branchOfficeRepository, branchTransform, messageUtil);
        sampleBranchOffice = BranchOffice.builder()
                .idBranchOffice(1L)
                .address("street11")
                .branchPhone(3123196836L)
                .logo("logo.png")
                .admin(1144201661L)
                .idVeterinaria(9L)
                .build();

        dataTest = new ArrayList<>();
        dataTest.add(sampleBranchOffice);

    }

    @Test
    void insertBranchOfficeValidFields() {
        when(branchOfficeRepository.save(Mockito.any(BranchOffice.class))).thenReturn(sampleBranchOffice);
        BranchOfficeDto BranchOffice = BranchOfficeDto.builder()
                .idBranchOffice(null)
                .address("street11")
                .branchPhone(3123196836L)
                .logo("logo.png")
                .admin(1144201661L)
                .idVeterinary(9L)
                .build();
        BranchOfficeDto actualMessage = branchOfficeServices.createBranchOffice(BranchOffice);
        assertThat(actualMessage).isNotNull();
    }

    @Test
    void insertBranchOfficeIdFieldNotNull() {
        when(branchOfficeRepository.save(Mockito.any(BranchOffice.class))).thenReturn(sampleBranchOffice);
        BranchOfficeDto BranchOffice = BranchOfficeDto.builder()
                .idBranchOffice(2L)
                .address("street11")
                .branchPhone(3123196836L)
                .logo("logo.png")
                .admin(1144201661L)
                .idVeterinary(9L)
                .build();
        Exception exception = assertThrows(BranchOfficeException.class, () -> branchOfficeServices.createBranchOffice(BranchOffice));
        String actualMessage = exception.getMessage();
        String expectedMessage = "campo id erroneo";
        assertThat(actualMessage).contains(expectedMessage);
    }

    @Test
    void insertBranchOfficeNullFields() {
        when(branchOfficeRepository.save(Mockito.any(BranchOffice.class))).thenReturn(sampleBranchOffice);
        BranchOfficeDto BranchOffice = BranchOfficeDto.builder()
                .idBranchOffice(null)
                .address(null)
                .branchPhone(null)
                .logo(null)
                .admin(null)
                .idVeterinary(null)
                .build();
        Exception exception = assertThrows(BranchOfficeException.class, () -> branchOfficeServices.createBranchOffice(BranchOffice));
        String actualMessage = exception.getMessage();
        String expectedMessage = "los campos están vacios";
        assertThat(actualMessage).contains(expectedMessage);
    }

    @Test
    void insertBranchOfficeLogoNullFields() {
        when(branchOfficeRepository.save(Mockito.any(BranchOffice.class))).thenReturn(sampleBranchOffice);
        BranchOfficeDto BranchOffice = BranchOfficeDto.builder()
                .idBranchOffice(null)
                .address("carrera")
                .branchPhone(3L)
                .logo(null)
                .admin(null)
                .idVeterinary(null)
                .build();
        Exception exception = assertThrows(BranchOfficeException.class, () -> branchOfficeServices.createBranchOffice(BranchOffice));
        String actualMessage = exception.getMessage();
        String expectedMessage = "los campos están vacios";
        assertThat(actualMessage).contains(expectedMessage);
    }

    @Test
    void insertBranchOfficePhoneNullFields() {
        when(branchOfficeRepository.save(Mockito.any(BranchOffice.class))).thenReturn(sampleBranchOffice);
        BranchOfficeDto BranchOffice = BranchOfficeDto.builder()
                .idBranchOffice(null)
                .address("carrera")
                .branchPhone(null)
                .logo("link")
                .admin(null)
                .idVeterinary(null)
                .build();
        Exception exception = assertThrows(BranchOfficeException.class, () -> branchOfficeServices.createBranchOffice(BranchOffice));
        String actualMessage = exception.getMessage();
        String expectedMessage = "los campos están vacios";
        assertThat(actualMessage).contains(expectedMessage);
    }

    @Test
    void insertBranchOfficeNullAdminFields() {
        when(branchOfficeRepository.save(Mockito.any(BranchOffice.class))).thenReturn(sampleBranchOffice);
        BranchOfficeDto BranchOffice = BranchOfficeDto.builder()
                .idBranchOffice(null)
                .address("carrera")
                .branchPhone(3L)
                .logo("link")
                .admin(null)
                .idVeterinary(null)
                .build();
        Exception exception = assertThrows(BranchOfficeException.class, () -> branchOfficeServices.createBranchOffice(BranchOffice));
        String actualMessage = exception.getMessage();
        String expectedMessage = "los campos están vacios";
        assertThat(actualMessage).contains(expectedMessage);
    }

    @Test
    void insertBranchOfficeVeterinaryNullFields() {
        when(branchOfficeRepository.save(Mockito.any(BranchOffice.class))).thenReturn(sampleBranchOffice);
        BranchOfficeDto BranchOffice = BranchOfficeDto.builder()
                .idBranchOffice(null)
                .address("carrera")
                .branchPhone(3L)
                .logo("link")
                .admin(34L)
                .idVeterinary(null)
                .build();
        Exception exception = assertThrows(BranchOfficeException.class, () -> branchOfficeServices.createBranchOffice(BranchOffice));
        String actualMessage = exception.getMessage();
        String expectedMessage = "los campos están vacios";
        assertThat(actualMessage).contains(expectedMessage);
    }

    @Test
    void insertNullBranchOffice() {
        when(branchOfficeRepository.save(Mockito.any(BranchOffice.class))).thenReturn(sampleBranchOffice);
        Exception exception = assertThrows(BranchOfficeException.class, () -> branchOfficeServices.createBranchOffice(null));
        String actualMessage = exception.getMessage();
        String expectedMessage = "Error al construir.";
        assertThat(actualMessage).isEqualTo(expectedMessage);
    }

    @Test
    void insertBranchOfficeValueFieldZero() {
        when(branchOfficeRepository.save(Mockito.any(BranchOffice.class))).thenReturn(sampleBranchOffice);
        BranchOfficeDto BranchOffice = BranchOfficeDto.builder()
                .idBranchOffice(null)
                .address("street11")
                .branchPhone(0L)
                .logo("logo.png")
                .admin(1144201661L)
                .idVeterinary(9L)
                .build();
        Exception exception = assertThrows(BranchOfficeException.class, () -> branchOfficeServices.createBranchOffice(BranchOffice));
        String actualMessage = exception.getMessage();
        String expectedMessage = "el valor ingresado no esta permitido";
        assertThat(actualMessage).isEqualTo(expectedMessage);
    }

    @Test
    void insertBranchOfficeValueAdminFieldZero() {
        when(branchOfficeRepository.save(Mockito.any(BranchOffice.class))).thenReturn(sampleBranchOffice);
        BranchOfficeDto BranchOffice = BranchOfficeDto.builder()
                .idBranchOffice(null)
                .address("street11")
                .branchPhone(2L)
                .logo("logo.png")
                .admin(0L)
                .idVeterinary(9L)
                .build();
        Exception exception = assertThrows(BranchOfficeException.class, () -> branchOfficeServices.createBranchOffice(BranchOffice));
        String actualMessage = exception.getMessage();
        String expectedMessage = "el valor ingresado no esta permitido";
        assertThat(actualMessage).isEqualTo(expectedMessage);
    }

    @Test
    void insertBranchOfficeValueFieldIdVeterinaryZero() {
        when(branchOfficeRepository.save(Mockito.any(BranchOffice.class))).thenReturn(sampleBranchOffice);
        BranchOfficeDto BranchOffice = BranchOfficeDto.builder()
                .idBranchOffice(null)
                .address("street11")
                .branchPhone(2L)
                .logo("logo.png")
                .admin(1144201661L)
                .idVeterinary(0L)
                .build();
        Exception exception = assertThrows(BranchOfficeException.class, () -> branchOfficeServices.createBranchOffice(BranchOffice));
        String actualMessage = exception.getMessage();
        String expectedMessage = "el valor ingresado no esta permitido";
        assertThat(actualMessage).isEqualTo(expectedMessage);
    }

    @Test
    void updateBranchOfficeValidFields() {
        when(branchOfficeRepository.save(Mockito.any(BranchOffice.class))).thenReturn(sampleBranchOffice);
        BranchOfficeDto BranchOffice = BranchOfficeDto.builder()
                .idBranchOffice(2L)
                .address("street12")
                .branchPhone(312319786L)
                .logo("logo.png")
                .admin(1144201661L)
                .idVeterinary(9L)
                .build();
        BranchOfficeDto actualMessage = branchOfficeServices.updateBranchOffice(BranchOffice);
        assertThat(actualMessage).isNotNull();
    }

    @Test
    void updateBranchOfficeIdFieldNull() {
        when(branchOfficeRepository.save(Mockito.any(BranchOffice.class))).thenReturn(sampleBranchOffice);
        BranchOfficeDto BranchOffice = BranchOfficeDto.builder()
                .idBranchOffice(null)
                .address("street12")
                .branchPhone(312319786L)
                .logo("logo.png")
                .admin(1144201661L)
                .idVeterinary(9L)
                .build();
        Exception exception = assertThrows(BranchOfficeException.class, () -> branchOfficeServices.updateBranchOffice(BranchOffice));
        String actualMessage = exception.getMessage();
        String expectedMessage = "campo id erroneo";
        assertThat(actualMessage).contains(expectedMessage);
    }

    @Test
    void updateBranchOfficeNullFields() {
        when(branchOfficeRepository.save(Mockito.any(BranchOffice.class))).thenReturn(sampleBranchOffice);
        BranchOfficeDto BranchOffice = BranchOfficeDto.builder()
                .idBranchOffice(null)
                .address(null)
                .branchPhone(null)
                .logo(null)
                .admin(null)
                .idVeterinary(null)
                .build();
        Exception exception = assertThrows(BranchOfficeException.class, () -> branchOfficeServices.updateBranchOffice(BranchOffice));
        String actualMessage = exception.getMessage();
        String expectedMessage = "campo id erroneo";
        assertThat(actualMessage).contains(expectedMessage);
    }

    @Test
    void updateNullBranchOffice() {
        when(branchOfficeRepository.save(Mockito.any(BranchOffice.class))).thenReturn(sampleBranchOffice);
        Exception exception = assertThrows(BranchOfficeException.class, () -> branchOfficeServices.updateBranchOffice(null));
        String actualMessage = exception.getMessage();
        String expectedMessage = "Error al construir.";
        assertThat(actualMessage).isEqualTo(expectedMessage);
    }

    @Test
    void updateBranchOfficeValueFieldZero() {
        when(branchOfficeRepository.save(Mockito.any(BranchOffice.class))).thenReturn(sampleBranchOffice);
        BranchOfficeDto BranchOffice = BranchOfficeDto.builder()
                .idBranchOffice(1L)
                .address("street12")
                .branchPhone(0L)
                .logo("logo.png")
                .admin(1144201661L)
                .idVeterinary(9L)
                .build();
        Exception exception = assertThrows(BranchOfficeException.class, () -> branchOfficeServices.updateBranchOffice(BranchOffice));
        String actualMessage = exception.getMessage();
        String expectedMessage = "el valor ingresado no esta permitido";
        assertThat(actualMessage).isEqualTo(expectedMessage);
    }

    @Test
    void getAllBranchOffice() {
        when(branchOfficeRepository.findAll()).thenReturn(dataTest);
        List<BranchOfficeDto> BranchOffices = branchOfficeServices.getAllBranchOffice();
        int actualSize = BranchOffices.size();
        assertTrue(actualSize > 0);
    }

    @Test
    void getAllBranchOfficeByNameExist() {
        when(branchOfficeRepository.findAll()).thenReturn(dataTest);
        List<BranchOfficeDto> BranchOffices = branchOfficeServices.getAllBranchOffice();
        int actualSize = BranchOffices.size();
        assertThat(actualSize).isPositive();
    }

    @Test
    void deleteBranchOfficeNullId() {
        Exception exception = assertThrows(BranchOfficeException.class,
                () -> branchOfficeServices.deleteBranchOffice(null));
        String actualMessage = exception.getMessage();
        String expectedMessage = "campo id erroneo";
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void deleteBranchOfficeId() {
        when(branchOfficeRepository.findById(1L)).thenReturn(Optional.of(sampleBranchOffice));
        String actualMessage = branchOfficeServices.deleteBranchOffice(1L);
        verify(branchOfficeRepository, times(1)).deleteById(1L);
        String expectedMessage = "Se ha eliminado exisitosamente";
        assertTrue(actualMessage.contains(expectedMessage));
    }
}
