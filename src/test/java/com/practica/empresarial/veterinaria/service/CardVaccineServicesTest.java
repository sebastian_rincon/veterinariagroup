package com.practica.empresarial.veterinaria.service;

import com.practica.empresarial.veterinaria.exceptions.CardVaccineException;
import com.practica.empresarial.veterinaria.factorie.CardVaccineFactory;
import com.practica.empresarial.veterinaria.factorie.DetailVaccineFactory;
import com.practica.empresarial.veterinaria.model.CardVaccine;
import com.practica.empresarial.veterinaria.model.dto.CardVaccineDto;
import com.practica.empresarial.veterinaria.model.dto.DetailVaccineDto;
import com.practica.empresarial.veterinaria.model.dto.MailBodyDto;
import com.practica.empresarial.veterinaria.model.transform.CardVaccineTransform;
import com.practica.empresarial.veterinaria.repository.CardVaccineRepository;
import com.practica.empresarial.veterinaria.utils.MailUtil;
import com.practica.empresarial.veterinaria.utils.MessageUtil;
import com.practica.empresarial.veterinaria.utils.PdfUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.FileSystemResource;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@SpringBootTest
class CardVaccineServicesTest {

    @Autowired
    MessageUtil messageUtil;

    PdfUtil pdfUtil;
    MailUtil mailUtil;
    DetailVaccineServices detailVaccineServices;
    CardVaccineRepository cardVaccineRepository;
    CardVaccineServices cardVaccineServices;
    CardVaccineTransform cardVaccineTransform;
    CardVaccine sampleCardVaccineModel;
    DetailVaccineDto sampleDetailVaccine;
    CardVaccineDto sampleCardVaccine;
    CardVaccineDto sampleCardVaccineCreate;
    CardVaccineDto sampleCardVaccineUpdate;
    Optional<CardVaccine> optionalCardVaccine;
    List<CardVaccine> sampleDataCardVaccine;
    List<DetailVaccineDto> sampleDataCardVaccineDto;
    Map<String, Object> data;

    @BeforeEach
    void setUp() {
        cardVaccineRepository = mock(CardVaccineRepository.class);
        mailUtil = mock(MailUtil.class);
        detailVaccineServices = mock(DetailVaccineServices.class);
        pdfUtil = mock(PdfUtil.class);
        cardVaccineTransform = new CardVaccineTransform();
        cardVaccineServices = new CardVaccineServices(cardVaccineRepository, cardVaccineTransform, messageUtil, detailVaccineServices, mailUtil, pdfUtil);
        sampleCardVaccineModel = new CardVaccineFactory().newInstance();
        sampleCardVaccine = new CardVaccineFactory().newInstanceDto();
        sampleCardVaccineCreate = new CardVaccineFactory().setIdCardVaccine(null).newInstanceDto();
        sampleCardVaccineUpdate = new CardVaccineFactory().newInstanceDto();
        sampleDetailVaccine = new DetailVaccineFactory().newInstanceDto();
        optionalCardVaccine = Optional.of(sampleCardVaccineModel);
        sampleDataCardVaccine = new ArrayList<>();
        sampleDataCardVaccineDto = new ArrayList<>();
        sampleDataCardVaccine.add(sampleCardVaccineModel);
        sampleDataCardVaccineDto.add(sampleDetailVaccine);
        data = new HashMap<>();
    }

    @Test
    void getOneByIdPet() {
        when(cardVaccineRepository.findByIdPet(Mockito.anyInt())).thenReturn(Optional.ofNullable(sampleCardVaccineModel));
        CardVaccineDto actualAnswer = cardVaccineServices.getOneByIdPet(1);
        assertThat(actualAnswer).isNotNull();
    }

    @Test
    void getOneByIdPetNull() {
        when(cardVaccineRepository.findByIdPet(Mockito.anyInt())).thenReturn(Optional.ofNullable(sampleCardVaccineModel));
        CardVaccineDto actualAnswer = cardVaccineServices.getOneByIdPet(null);
        assertThat(actualAnswer).isNull();
    }

//    @Test
//    void testSendEmailWithPDForParams() {
//        when(cardVaccineRepository.findByIdPet(Mockito.anyInt())).thenReturn(optionalCardVaccine);
//        when(detailVaccineServices.getAllByIdCardVaccine(Mockito.anyInt())).thenReturn(sampleDataCardVaccineDto);
//        doNothing().when(mailUtil).sendEmailWithAttachment(Mockito.any(MailBodyDto.class), Mockito.anyString(), Mockito.any(FileSystemResource.class));
//        cardVaccineServices.sendByEmailCardVaccine("sebas199765@gmail.com", 1);
//    }


    @Test
    void getCardVaccine() {
        when(cardVaccineRepository.findByIdPet(Mockito.anyInt())).thenReturn(optionalCardVaccine);
        when(detailVaccineServices.getAllByIdCardVaccine(Mockito.anyInt())).thenReturn(sampleDataCardVaccineDto);
        Map<String, Object> consul = cardVaccineServices.getCardVaccine(1);
        assertThat(consul).isNotEmpty();
    }

    @Test
    void testSendEmailWithPDForParamsNull() {
        when(cardVaccineRepository.findByIdPet(Mockito.anyInt())).thenReturn(optionalCardVaccine);
        when(detailVaccineServices.getAllByIdCardVaccine(Mockito.anyInt())).thenReturn(sampleDataCardVaccineDto);
        doNothing().when(mailUtil).sendEmailWithAttachment(Mockito.any(MailBodyDto.class), Mockito.anyString(), Mockito.any(FileSystemResource.class));
        Exception exception = assertThrows(CardVaccineException.class, () -> cardVaccineServices.sendByEmailCardVaccine(null, null));
        String expectedMessage = "el campo está vacio";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

}