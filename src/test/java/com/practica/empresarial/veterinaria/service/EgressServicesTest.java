package com.practica.empresarial.veterinaria.service;

import com.practica.empresarial.veterinaria.exceptions.EgressException;
import com.practica.empresarial.veterinaria.factorie.EgressFactory;
import com.practica.empresarial.veterinaria.model.Egress;
import com.practica.empresarial.veterinaria.model.dto.EgressDto;
import com.practica.empresarial.veterinaria.model.transform.EgressTransform;
import com.practica.empresarial.veterinaria.repository.EgressRepository;
import com.practica.empresarial.veterinaria.utils.MessageUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SpringBootTest
class EgressServicesTest {

    @Autowired
    private MessageUtil messageUtil;
    private EgressServices egressServices;
    private EgressRepository egressRepository;

    private Egress sampleEgress;
    private EgressDto sampleEgressDtoCreate;
    private EgressDto sampleEgressDto;
    private List<Egress> sampleDataTest;
    private List<Egress> sampleDataTestEmpty;

    @BeforeEach
    public void setup() {
        egressRepository = mock(EgressRepository.class);
        EgressTransform egressTransform = new EgressTransform();

        egressServices = new EgressServices(egressRepository, egressTransform);
        sampleEgress = new EgressFactory().newInstance();
        sampleEgressDto = new EgressFactory().newInstanceDto();
        sampleEgressDtoCreate = new EgressFactory().setIdEgress(null).newInstanceDto();

        sampleDataTest = new ArrayList<>();
        sampleDataTestEmpty = new ArrayList<>();
        sampleDataTest.add(sampleEgress);
    }

    @Test
    void getExistingType() {
        when(egressRepository.findAllByType(sampleEgress.getType())).thenReturn(sampleDataTest);
        String type = "medicamento";
        List<EgressDto> dataConsulted = egressServices.getAllEgressByType(type);
        assertThat(dataConsulted).isNotEmpty();
    }

    @Test
    void getNoExistingType() {
        String type = "deportes";
        when(egressRepository.findAllByType(sampleEgress.getType())).thenReturn(sampleDataTestEmpty);
        List<EgressDto> dataConsulted = egressServices.getAllEgressByType(type);
        assertThat(dataConsulted).isEmpty();
    }

    @Test
    void getExistingPet() {
        when(egressRepository.findAllByIdPet(Mockito.anyLong())).thenReturn(sampleDataTest);
        Long pet = 1L;
        List<EgressDto> dataConsulted = egressServices.getAllEgressByPet(pet);
        assertThat(dataConsulted).isNotEmpty();
    }

    @Test
    void getNoExistingPet() {
        Long pet = 999L;
        when(egressRepository.findAllByType(sampleEgress.getType())).thenReturn(sampleDataTestEmpty);
        List<EgressDto> dataConsulted = egressServices.getAllEgressByPet(pet);
        assertThat(dataConsulted).isEmpty();
    }


    @Test
    void getAll() {
        when(egressRepository.findAll()).thenReturn(sampleDataTest);
        List<EgressDto> dataConsulted = egressServices.getAllEgress();
        assertThat(dataConsulted).isNotEmpty();
    }


    @Test
    void getExistingTypeToValidateObject() {
        when(egressRepository.findAll()).thenReturn(sampleDataTest);
        String type = "medicamento";
        List<EgressDto> dataConsulted = egressServices.getAllEgressByType(type);
        for (EgressDto egress : dataConsulted) {
            assertThat(egress.getType()).isEqualTo(type);
        }
    }

    @Test
    void getNullType() {
        when(egressRepository.findAll()).thenReturn(sampleDataTestEmpty);
        List<EgressDto> dataConsulted = egressServices.getAllEgressByType(null);
        assertThat(dataConsulted).isEmpty();
    }

    @Test
    void insertEgressCorrectFields() {
        when(egressRepository.save(Mockito.any(Egress.class))).thenReturn(sampleEgress);
        EgressDto actualAnswer = egressServices.createEgress(sampleEgressDtoCreate);
        assertThat(actualAnswer.getIdEgress()).isEqualTo(sampleEgressDto.getIdEgress());
    }

    @Test
    void insertEgressIdFieldNull() {
        EgressDto egress = new EgressFactory().setIdEgress(5L).newInstanceDto();
        Exception exception = assertThrows(EgressException.class, () -> egressServices.createEgress(egress));
        String actualMessage = exception.getMessage();
        String expectedMessage = "campo id erroneo";
        assertThat(actualMessage).contains(expectedMessage);
    }

    @Test
    void setCreateEgressNullFields() {
        EgressDto egress = new EgressFactory().setIdEgress(null).setDescription(null).newInstanceDto();
        Exception exception = assertThrows(EgressException.class, () -> egressServices.createEgress(egress));
        String actualMessage = exception.getMessage();
        String expectedMessage = "los campos están vacios";
        assertThat(actualMessage).contains(expectedMessage);
    }

    @Test
    void setCreateEgressNullConceptFields() {
        EgressDto egress = new EgressFactory().setIdEgress(null).setConcept(null).newInstanceDto();
        Exception exception = assertThrows(EgressException.class, () -> egressServices.createEgress(egress));
        String actualMessage = exception.getMessage();
        String expectedMessage = "los campos están vacios";
        assertThat(actualMessage).contains(expectedMessage);
    }

    @Test
    void setCreateEgressNullTypeFields() {
        EgressDto egress = new EgressFactory().setIdEgress(null).setType(null).newInstanceDto();
        Exception exception = assertThrows(EgressException.class, () -> egressServices.createEgress(egress));
        String actualMessage = exception.getMessage();
        String expectedMessage = "los campos están vacios";
        assertThat(actualMessage).contains(expectedMessage);
    }


    @Test
    void setCreateEgressNullDateFields() {
        EgressDto egress = new EgressFactory().setIdEgress(null).setDate(null).newInstanceDto();
        Exception exception = assertThrows(EgressException.class, () -> egressServices.createEgress(egress));
        String actualMessage = exception.getMessage();
        String expectedMessage = "los campos están vacios";
        assertThat(actualMessage).contains(expectedMessage);
    }

    @Test
    void setCreateEgressNullValueFields() {
        EgressDto egress = new EgressFactory().setIdEgress(null).setValue(null).newInstanceDto();
        Exception exception = assertThrows(EgressException.class, () -> egressServices.createEgress(egress));
        String actualMessage = exception.getMessage();
        String expectedMessage = "los campos están vacios";
        assertThat(actualMessage).contains(expectedMessage);
    }

    @Test
    void insertNullEgress() {
        when(egressRepository.save(sampleEgress)).thenThrow(IllegalArgumentException.class);
        Exception exception = assertThrows(EgressException.class, () -> egressServices.createEgress(null));
        String actualMessage = exception.getMessage();
        String expectedMessage = "Error al construir.";
        assertThat(actualMessage).contains(expectedMessage);
    }

    @Test
    void insertEgressValueFieldZero() {
        EgressDto egress = new EgressFactory().setIdEgress(null).setValue(0F).newInstanceDto();
        Exception exception = assertThrows(EgressException.class, () -> egressServices.createEgress(egress));
        String actualMessage = exception.getMessage();
        String expectedMessage = "el valor ingresado no esta permitido";
        assertThat(actualMessage).isEqualTo(expectedMessage);
    }

    @Test
    void updateEgressCorrectFields() {
        when(egressRepository.save(Mockito.any(Egress.class))).thenReturn(sampleEgress);
        EgressDto egress = new EgressFactory().newInstanceDto();
        EgressDto actualAnswer = egressServices.updateEgress(egress);
        assertThat(actualAnswer).isNotNull();
    }

    @Test
    void updateEgressIdFieldNull() {
        when(egressRepository.save(Mockito.any(Egress.class))).thenReturn(sampleEgress);
        EgressDto egress = new EgressFactory().setIdEgress(null).newInstanceDto();
        Exception exception = assertThrows(EgressException.class, () -> egressServices.updateEgress(egress));
        String actualMessage = exception.getMessage();
        String expectedMessage = "campo id erroneo";
        assertThat(actualMessage).contains(expectedMessage);
    }

    @Test
    void setEgressNullFields() {
        when(egressRepository.save(Mockito.any(Egress.class))).thenReturn(sampleEgress);
        EgressDto egress = new EgressFactory().setDescription(null).newInstanceDto();
        Exception exception = assertThrows(EgressException.class, () -> egressServices.updateEgress(egress));
        String actualMessage = exception.getMessage();
        String expectedMessage = "los campos están vacios";
        assertThat(actualMessage).contains(expectedMessage);
    }

    @Test
    void updateNullEgress() {
        when(egressRepository.save(Mockito.any(Egress.class))).thenReturn(sampleEgress);
        Exception exception = assertThrows(EgressException.class, () -> egressServices.updateEgress(null));
        String actualMessage = exception.getMessage();
        String expectedMessage = "Error al construir.";
        assertThat(actualMessage).contains(expectedMessage);
    }

    @Test
    void updateEgressValueFieldZero() {
        when(egressRepository.save(Mockito.any(Egress.class))).thenReturn(sampleEgress);
        EgressDto egress = new EgressFactory().setValue(0F).newInstanceDto();
        Exception exception = assertThrows(EgressException.class, () -> egressServices.updateEgress(egress));
        String actualMessage = exception.getMessage();
        String expectedMessage = "el valor ingresado no esta permitido";
        assertThat(actualMessage).isEqualTo(expectedMessage);
    }

}
