package com.practica.empresarial.veterinaria.service;

import com.practica.empresarial.veterinaria.factorie.AppointmentFactory;
import com.practica.empresarial.veterinaria.model.Appointment;
import com.practica.empresarial.veterinaria.model.dto.AppointmentDto;
import com.practica.empresarial.veterinaria.model.transform.AppointmentTransform;
import com.practica.empresarial.veterinaria.repository.AppointmentRepository;
import com.practica.empresarial.veterinaria.utils.MessageUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SpringBootTest
class AppointmentServiceTest {

    public static final String CONSULT = "consulta";
    public static final String CONSULTWO = "musica";

    @Autowired
    private MessageUtil messageUtil;
    private AppointmentService appointmentService;
    private AppointmentRepository appointmentRepository;

    private Appointment sampleAppointment;
    private AppointmentDto sampleAppointmentDto;
    private List<Appointment> sampleDataTest;
    private List<Appointment> sampleDataTestEmpty;

    @BeforeEach
    public void setup() {
        appointmentRepository = mock(AppointmentRepository.class);
        AppointmentTransform appointmentTransform = new AppointmentTransform();

        appointmentService = new AppointmentService(appointmentRepository, appointmentTransform);
        sampleAppointment = new AppointmentFactory().newInstance();
        sampleAppointmentDto = new AppointmentFactory().newInstanceDto();

        sampleDataTest = new ArrayList<>();
        sampleDataTestEmpty = new ArrayList<>();
        sampleDataTest.add(sampleAppointment);
    }


    @Test
    void getExistingTopic() {
        when(appointmentRepository.findAllByTopicAppointment(Mockito.anyString())).thenReturn(sampleDataTest);
        List<AppointmentDto> dataConsulted = appointmentService.getAllByTopicAppointment(CONSULT);
        assertThat(dataConsulted).isNotEmpty();
        assertThat(dataConsulted.get(0).getIdPet()).isEqualTo(sampleDataTest.get(0).getIdPet());
    }

    @Test
    void getNoExistingTopic() {
        when(appointmentRepository.findAllByTopicAppointment(Mockito.anyString())).thenReturn(sampleDataTestEmpty);
        List<AppointmentDto> dataConsulted = appointmentService.getAllByTopicAppointment(CONSULTWO);
        assertThat(dataConsulted).isEmpty();
    }
}