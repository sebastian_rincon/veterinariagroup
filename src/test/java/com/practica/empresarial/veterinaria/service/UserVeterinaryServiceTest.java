package com.practica.empresarial.veterinaria.service;


import com.practica.empresarial.veterinaria.exceptions.UserException;
import com.practica.empresarial.veterinaria.factorie.UserVeterinaryFactory;
import com.practica.empresarial.veterinaria.model.UserVeterinary;
import com.practica.empresarial.veterinaria.model.dto.UserVeterinaryDto;
import com.practica.empresarial.veterinaria.model.transform.UserVeterinaryTransform;
import com.practica.empresarial.veterinaria.repository.UserVeterinaryRepository;
import com.practica.empresarial.veterinaria.utils.MessageUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SpringBootTest
class UserVeterinaryServiceTest {

    @Autowired
    private MessageUtil messageUtil;
    private UserVeterinaryService userVeterinaryService;
    private UserVeterinaryRepository userVeterinaryRepository;

    private UserVeterinary sampleUser;
    private UserVeterinaryDto sampleUserDto;
    private Optional<UserVeterinary> sampleDataTest;
    private Optional<UserVeterinary> sampleDataTestEmpty;

    @BeforeEach
    public void setup() {
        userVeterinaryRepository = mock(UserVeterinaryRepository.class);
        UserVeterinaryTransform userVeterinaryTransform = new UserVeterinaryTransform();

        userVeterinaryService = new UserVeterinaryService(userVeterinaryRepository, messageUtil, userVeterinaryTransform);
        sampleUser = new UserVeterinaryFactory().newInstance();
        sampleUserDto = new UserVeterinaryFactory().newInstanceDto();

        sampleDataTest = Optional.of(sampleUser);
        sampleDataTestEmpty = Optional.empty();
    }

    @Test
    void getUserByUserNameNullUserName() {
        when(userVeterinaryRepository.findByUserName(null)).thenThrow(IllegalArgumentException.class);
        Exception exception = assertThrows(UserException.class, () -> userVeterinaryService.getUserByUserName(null));
        String actualMessage = exception.getMessage();
        String expectedMessage = "el campo está vacio";
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void getUserByUserNameNotNull() {
        when(userVeterinaryRepository.findByUserName(Mockito.any(String.class))).thenReturn(sampleDataTest);
        UserVeterinaryDto user = userVeterinaryService.getUserByUserName("south97");
        assertThat(user).isNotNull();
    }


}