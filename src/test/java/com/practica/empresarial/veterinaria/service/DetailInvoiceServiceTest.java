package com.practica.empresarial.veterinaria.service;

import com.practica.empresarial.veterinaria.factorie.DetailInvoiceFactory;
import com.practica.empresarial.veterinaria.model.DetailInvoice;
import com.practica.empresarial.veterinaria.model.dto.DetailInvoiceDto;
import com.practica.empresarial.veterinaria.model.transform.DetailInvoiceTransform;
import com.practica.empresarial.veterinaria.repository.DetailInvoiceRepository;
import com.practica.empresarial.veterinaria.utils.MessageUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class DetailInvoiceServiceTest {
    @Autowired
    private MessageUtil messageUtil;

    private DetailInvoiceService detailInvoiceService;
    private DetailInvoiceRepository detailInvoiceRepository;
    private DetailInvoiceDto sampleDetailInvoiceCreate;
    private DetailInvoice sampleDetailInvoice;
    private DetailInvoiceDto sampleDetailInvoiceDto;
    private List<DetailInvoice> sampleDataTest;
    private List<DetailInvoice> sampleDataTestEmpty;

    @BeforeEach
    public void setup() {
        detailInvoiceRepository = mock(DetailInvoiceRepository.class);
        DetailInvoiceTransform detailInvoiceTransform = new DetailInvoiceTransform();

        detailInvoiceService = new DetailInvoiceService(detailInvoiceTransform, detailInvoiceRepository);
        sampleDetailInvoice = new DetailInvoiceFactory().newInstance();
        sampleDetailInvoiceDto = new DetailInvoiceFactory().newInstanceDto();
        sampleDetailInvoiceCreate = new DetailInvoiceFactory().setId(null).newInstanceDto();

        sampleDataTest = new ArrayList<>();
        sampleDataTestEmpty = new ArrayList<>();
        sampleDataTest.add(sampleDetailInvoice);

    }

    @Test
    void getExistingPet() {
        Integer idInvoice = 0;
        when(detailInvoiceRepository.findAllByIdInvoice(sampleDetailInvoice.getIdInvoice())).thenReturn(sampleDataTestEmpty);
        List<DetailInvoiceDto> dataConsulted = detailInvoiceService.getAllByidInvoice(idInvoice);
        assertThat(dataConsulted).isEmpty();
    }

    @Test
    void getNoExistingPet() {
        when(detailInvoiceRepository.findAllByIdInvoice(sampleDetailInvoice.getIdInvoice())).thenReturn(sampleDataTest);
        Integer idInvoice = 0;
        List<DetailInvoiceDto> dataConsulted = detailInvoiceService.getAllByidInvoice(idInvoice);
        assertThat(dataConsulted).isEmpty();
    }


}