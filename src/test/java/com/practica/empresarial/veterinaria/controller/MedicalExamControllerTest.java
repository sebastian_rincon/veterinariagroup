package com.practica.empresarial.veterinaria.controller;

import com.practica.empresarial.veterinaria.factorie.MedicalExamFactory;
import com.practica.empresarial.veterinaria.model.MedicalExam;
import com.practica.empresarial.veterinaria.model.dto.MedicalExamDto;
import com.practica.empresarial.veterinaria.service.MedicalExamServices;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SpringBootTest
public class MedicalExamControllerTest {
    private MedicalExamController medicalExamController;
    private MedicalExamServices medicalExamServices;

    private MedicalExam medicalExam;
    private MedicalExamDto sampleMedicalExamUpdateDto;
    private List<MedicalExamDto> sampleDataTest;

    @BeforeEach
    public void setup() {
        medicalExamServices = mock(MedicalExamServices.class);
        medicalExamController = new MedicalExamController(medicalExamServices);

        sampleMedicalExamUpdateDto = new MedicalExamFactory().newInstanceDto();

        sampleDataTest = new ArrayList<>();
        sampleDataTest.add(sampleMedicalExamUpdateDto);
    }


    @Test
    void testControllerSendEmail() {
        when(medicalExamServices.sendByEmailMedicalExamReport(Mockito.anyString(), Mockito.anyLong())).thenReturn("string");
        String medExam = medicalExamController.sendMailPDF("sample@email.com", 1L);
        assertThat(medExam).isNotBlank();
    }

}
