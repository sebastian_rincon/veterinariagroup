package com.practica.empresarial.veterinaria.controller;

import com.practica.empresarial.veterinaria.model.dto.AppointmentDto;
import com.practica.empresarial.veterinaria.service.AppointmentService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


@SpringBootTest
class AppointmentControllerTest {

    AppointmentController appointmentController;
    AppointmentService appointmentService;

    AppointmentDto sampleAppointmentUpdateDto;
    private List<AppointmentDto> sampleDataTest;

    @BeforeEach
    public void setup() {
        appointmentService = mock(AppointmentService.class);
        appointmentController = new AppointmentController(appointmentService);

        sampleAppointmentUpdateDto = AppointmentDto.builder()
                .idAppointment(1L)
                .dateTimeAppointment(new Timestamp(new java.util.Date().getTime()))
                .stateAppointment(true)
                .topicAppointment("consulta")
                .idPet(1)
                .build();

        sampleDataTest = new ArrayList<>();
        sampleDataTest.add(sampleAppointmentUpdateDto);
    }

    @Test
    void testControllerGetTopicAppointment() {
        when(appointmentService.getAllByTopicAppointment(sampleAppointmentUpdateDto.getTopicAppointment())).thenReturn(sampleDataTest);
        List<AppointmentDto> appointment = appointmentController.getAllByTopicAppointment("consulta");
        assertThat(appointment).isNotEmpty();
    }
}