package com.practica.empresarial.veterinaria.controller;

import com.practica.empresarial.veterinaria.model.dto.ClinicalHistoryDetailDto;
import com.practica.empresarial.veterinaria.service.ClinicalHistoryDetailServices;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class ClinicalHistoryDetailControllerTest {

    ClinicalHistoryDetailController clinicalHistoryController;
    ClinicalHistoryDetailServices clinicalHistoryServices;

    ClinicalHistoryDetailDto clinicalHistoryDtoUpdate;
    ClinicalHistoryDetailDto clinicalHistoryDtoCreate;

    @BeforeEach
    public void setup() {
        clinicalHistoryServices = mock(ClinicalHistoryDetailServices.class);
        clinicalHistoryController = new ClinicalHistoryDetailController(clinicalHistoryServices);

        clinicalHistoryDtoUpdate = ClinicalHistoryDetailDto.builder()
                .idDetailHC(2L)
                .temperature(33f)
                .weight(40f)
                .heartRate(22.5f)
                .breathingFrequency(20.4f)
                .admissionDate(new Date())
                .idClinicalHistory(2L)
                .feeding("croquetas")
                .dewormingDate(new Date())
                .habitat("hogar")
                .capillaryTime(30f)
                .reproductiveStatus("castrado")
                .observations("mascota en buen estado de salud")
                .idCollaborator(3L)
                .build();


        clinicalHistoryDtoCreate = ClinicalHistoryDetailDto.builder()
                .idDetailHC(null)
                .temperature(33f)
                .weight(40f)
                .heartRate(22.5f)
                .breathingFrequency(20.4f)
                .admissionDate(new Date())
                .idClinicalHistory(2L)
                .feeding("croquetas")
                .dewormingDate(new Date())
                .habitat("hogar")
                .capillaryTime(30f)
                .reproductiveStatus("castrado")
                .observations("mascota en buen estado de salud")
                .idCollaborator(3L)
                .build();


        List<ClinicalHistoryDetailDto> sampleDataTest = new ArrayList<>();
        sampleDataTest.add(clinicalHistoryDtoCreate);

    }

    @Test
    void createClinicalHistory() {
        when(clinicalHistoryServices.createClinicalHistory(Mockito.any(ClinicalHistoryDetailDto.class))).thenReturn(clinicalHistoryDtoCreate);
        ClinicalHistoryDetailDto clinical = ClinicalHistoryDetailDto.builder()
                .idDetailHC(null)
                .temperature(33f)
                .weight(40f)
                .heartRate(22.5f)
                .breathingFrequency(20.4f)
                .admissionDate(new Date())
                .idClinicalHistory(2L)
                .feeding("croquetas")
                .dewormingDate(new Date())
                .habitat("hogar")
                .capillaryTime(30f)
                .reproductiveStatus("castrado")
                .observations("mascota en buen estado de salud")
                .idCollaborator(3L)
                .build();

        ClinicalHistoryDetailDto actualAnwser = clinicalHistoryController.createClinicalHistory(clinical);
        assertThat(actualAnwser).isNotNull();
    }
}