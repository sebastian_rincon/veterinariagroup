package com.practica.empresarial.veterinaria.controller;

import com.practica.empresarial.veterinaria.factorie.IncomeFactory;
import com.practica.empresarial.veterinaria.model.dto.IncomeDto;
import com.practica.empresarial.veterinaria.service.IncomeServices;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SpringBootTest
class IncomeControllerTest {

    private IncomeController incomeController;
    private IncomeServices incomeServices;
    private IncomeDto sampleIncomeUpdateDto;
    private List<IncomeDto> sampleDataTest;

    @BeforeEach
    private void setup() {
        incomeServices = mock(IncomeServices.class);
        incomeController = new IncomeController(incomeServices);
        sampleIncomeUpdateDto = new IncomeFactory().newInstanceDto();
        sampleDataTest = new ArrayList<>();
        sampleDataTest.add(sampleIncomeUpdateDto);
    }

    @Test
    void testControllerGetAll() {
        when(incomeServices.getAllIncome()).thenReturn(sampleDataTest);
        List<IncomeDto> income = incomeController.getAllIncome();
        when(incomeServices.getAllIncome()).thenReturn(sampleDataTest);
        assertThat(income).isNotEmpty();
    }

}
