package com.practica.empresarial.veterinaria.controller;

import com.practica.empresarial.veterinaria.model.dto.ClinicalHistoryDto;
import com.practica.empresarial.veterinaria.service.ClinicalHistoryServices;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SpringBootTest
class ClinicalHistoryControllerTest {

    ClinicalHistoryController clinicalHistoryController;
    ClinicalHistoryServices clinicalHistoryServices;

    ClinicalHistoryDto clinicalHistoryDtoUpdate;
    ClinicalHistoryDto clinicalHistoryDtoCreate;

    @BeforeEach
    public void setup() {
        clinicalHistoryServices = mock(ClinicalHistoryServices.class);
        clinicalHistoryController = new ClinicalHistoryController(clinicalHistoryServices);

        clinicalHistoryDtoUpdate = ClinicalHistoryDto.builder()
                .idClinicalHistory(1L)
                .dateCreate(new Date())
                .idPet(1L)
                .build();

        clinicalHistoryDtoCreate = ClinicalHistoryDto.builder()
                .idClinicalHistory(null)
                .dateCreate(new Date())
                .idPet(1L)
                .build();


    }

    @Test
    void createClinicalHistory() {
        when(clinicalHistoryServices.createClinicalHistory(Mockito.any(ClinicalHistoryDto.class))).thenReturn(clinicalHistoryDtoCreate);
        ClinicalHistoryDto clinical = ClinicalHistoryDto.builder()
                .idClinicalHistory(null)
                .dateCreate(new Date())
                .idPet(1L)
                .build();
        ClinicalHistoryDto actualAnwser = clinicalHistoryController.createClinicalHistory(clinical);
        assertThat(actualAnwser).isNotNull();
    }
}