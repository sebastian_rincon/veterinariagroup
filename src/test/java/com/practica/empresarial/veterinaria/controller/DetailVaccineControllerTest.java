package com.practica.empresarial.veterinaria.controller;

import com.practica.empresarial.veterinaria.model.dto.DetailVaccineDto;
import com.practica.empresarial.veterinaria.service.DetailVaccineServices;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


@SpringBootTest
class DetailVaccineControllerTest {

    DetailVaccineController detailVaccineController;
    DetailVaccineServices detailVaccineServices;

    DetailVaccineDto sampleDetailVaccineUpdateDto;
    private List<DetailVaccineDto> sampleDataTest;

    @BeforeEach
    public void setup() {
        detailVaccineServices = mock(DetailVaccineServices.class);
        detailVaccineController = new DetailVaccineController(detailVaccineServices);

        sampleDetailVaccineUpdateDto = DetailVaccineDto.builder()
                .idDetailVaccine(1L)
                .nameVaccine("hepatitis")
                .doseVaccine(15000F)
                .dateVaccine(new Date())
                .codeVaccine(15000F)
                .idCardVaccine(1)
                .state(true)
                .build();
        sampleDataTest = new ArrayList<>();
        sampleDataTest.add(sampleDetailVaccineUpdateDto);
    }

    @Test
    void testControllerGetAllIdCardVaccine() {
        when(detailVaccineServices.getAllByIdCardVaccine(sampleDetailVaccineUpdateDto.getIdCardVaccine())).thenReturn(sampleDataTest);
        List<DetailVaccineDto> detailVaccine = detailVaccineController.getAllByIdCardVaccine(1);
        assertThat(detailVaccine).isNotEmpty();
    }
}