package com.practica.empresarial.veterinaria.controller;


import com.practica.empresarial.veterinaria.model.dto.VeterinaryDto;
import com.practica.empresarial.veterinaria.service.VeterinaryServices;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SpringBootTest
class VeterinaryControllerTest {

    VeterinaryController veterinaryController;
    VeterinaryServices veterinaryServices;

    VeterinaryDto sampleVeterinaryDtoUpdate;
    VeterinaryDto sampleVeterinaryDtoCreate;
    private List<VeterinaryDto> sampleDataTest;

    @BeforeEach
    public void setup() {
        veterinaryServices = mock(VeterinaryServices.class);
        veterinaryController = new VeterinaryController(veterinaryServices);

        sampleVeterinaryDtoUpdate = VeterinaryDto.builder()
                .idVeterinary(2L)
                .name("zoopets5")
                .email("zoopets@gmail.com")
                .nit(1144201661L)
                .owner(5L)
                .status(true)
                .build();

        sampleVeterinaryDtoCreate = VeterinaryDto.builder()
                .idVeterinary(null)
                .name("zoopets")
                .email("zoopets@gmail.com")
                .nit(1144201661L)
                .owner(5L)
                .status(true)
                .build();

        sampleDataTest = new ArrayList<>();
        sampleDataTest.add(sampleVeterinaryDtoUpdate);

    }

    @Test
    void createCategory() {
        when(veterinaryServices.createVeterinary(Mockito.any(VeterinaryDto.class))).thenReturn(sampleVeterinaryDtoCreate);
        VeterinaryDto veterinary = VeterinaryDto.builder()
                .idVeterinary(null)
                .name("zoopets")
                .email("zoopets@gmail.com")
                .nit(1144201661L)
                .owner(5L)
                .status(true)
                .build();
        VeterinaryDto actualAnwser = veterinaryController.createVeterinary(veterinary);
        assertThat(actualAnwser).isNotNull();
    }

    @Test
    void updateCategory() {
        when(veterinaryServices.updateVeterinary(Mockito.any(VeterinaryDto.class))).thenReturn(sampleVeterinaryDtoUpdate);
        VeterinaryDto veterinary = VeterinaryDto.builder()
                .idVeterinary(1L)
                .name("zoopets2")
                .email("zoopets@gmail.com")
                .nit(1144201661L)
                .owner(5L)
                .status(true)
                .build();
        VeterinaryDto actualAnwser = veterinaryController.updateVeterinary(veterinary);
        assertThat(actualAnwser).isNotNull();
    }


    @Test
    void getAll() {
        when(veterinaryServices.getAllVeterinary()).thenReturn(sampleDataTest);
        List<VeterinaryDto> veterinaries = veterinaryController.getAll();
        int actualSize = veterinaries.size();
        assertTrue(actualSize > 0);
    }

    @Test
    void getAllByName() {

        when(veterinaryServices.getAllVeterinaryByName("zoopets")).thenReturn(sampleDataTest);
        String name = "zoopets";
        List<VeterinaryDto> veterinaries = veterinaryController.getAllByName(name);
        int actualSize = veterinaries.size();
        assertThat(actualSize).isPositive();
    }

    @Test
    void deleteById() {
        when(veterinaryServices.deleteVeterinary(1L)).thenReturn("Se ha eliminado exisitosamente");
        String actualMessage = veterinaryController.deleteById(1L);
        String expectedMessage = "Se ha eliminado exisitosamente";
        assertTrue(actualMessage.contains(expectedMessage));
    }
}