package com.practica.empresarial.veterinaria.controller;

import com.practica.empresarial.veterinaria.model.dto.ServiceVeterinaryDto;
import com.practica.empresarial.veterinaria.service.ServiceVeterinaryServices;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SpringBootTest
class ServiceVeterinaryControllerTest {

    ServiceVeterinaryController serviceVeterinaryController;
    ServiceVeterinaryServices serviceVeterinaryServices;

    ServiceVeterinaryDto sampleServicesVeterinaryUpdateDto;
    ServiceVeterinaryDto sampleServiceVeterinaryCreateDto;
    private List<ServiceVeterinaryDto> sampleDataTest;

    @BeforeEach
    public void setup() {
        serviceVeterinaryServices = mock(ServiceVeterinaryServices.class);
        serviceVeterinaryController = new ServiceVeterinaryController(serviceVeterinaryServices);

        sampleServicesVeterinaryUpdateDto = ServiceVeterinaryDto.builder()
                .idService(null)
                .nameService("Peluqueria")
                .descriptionService("Corte")
                .valueService(50f)
                .quantityService((long) 5)
                .statusService(true)
                .build();

        sampleServiceVeterinaryCreateDto = ServiceVeterinaryDto.builder()
                .idService(1L)
                .nameService("Peluqueria")
                .descriptionService("Corte")
                .valueService(50f)
                .quantityService((long) 5)
                .statusService(true)
                .build();

        sampleDataTest = new ArrayList<>();
        sampleDataTest.add(sampleServicesVeterinaryUpdateDto);

    }

    @Test
    void testControllerCreateServices() {

        when(serviceVeterinaryServices.createServices(Mockito.any(ServiceVeterinaryDto.class))).thenReturn(sampleServiceVeterinaryCreateDto);
        ServiceVeterinaryDto actualAnswer = serviceVeterinaryController.createServices(sampleServiceVeterinaryCreateDto);
        assertThat(actualAnswer).isNotNull();
    }

    @Test
    void testControllerUpdateServices() {

        when(serviceVeterinaryServices.updateService(Mockito.any(ServiceVeterinaryDto.class))).thenReturn(sampleServicesVeterinaryUpdateDto);
        ServiceVeterinaryDto actualAnswer = serviceVeterinaryController.updateServices(sampleServiceVeterinaryCreateDto);
        assertThat(actualAnswer).isNotNull();
    }

    @Test
    void findAllByName() {
        when(serviceVeterinaryServices.getAllServiceByNameService(sampleServicesVeterinaryUpdateDto.getNameService())).thenReturn(sampleDataTest);
        List<ServiceVeterinaryDto> datoConsultado = serviceVeterinaryController.getAllServiceByNameService("Peluqueria");
        assertThat(datoConsultado).isNotEmpty();
    }

    @Test
    void getAllServiceByType() {
        when(serviceVeterinaryServices.getAllServiceByType(1)).thenReturn(sampleDataTest);
        Integer type = 1;
        List<ServiceVeterinaryDto> dataConsul = serviceVeterinaryController.getAllServiceByType(type);
        assertThat(dataConsul).isNotEmpty();
    }

}